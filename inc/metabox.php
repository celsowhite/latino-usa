<?php
add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cmb_';

	/**
	 * Repeatable Field Groups
	 */
	$meta_boxes['contributors'] = array(
		'id'         => 'field_contributors',
		'title'      => 'Contributors',
		'pages'      => array('post'),
		'fields'     => array(
			array(
				'id'          => $prefix . 'contributor_group',
				'type'        => 'group',
				'description' => '',
				'options'     => array(
					'group_title'   => __( 'Contributor {#}', 'cmb' ), // {#} gets replaced by row number
					'add_button'    => __( 'Add Another Contributor', 'cmb' ),
					'remove_button' => __( 'Remove Contributor', 'cmb' ),
					'sortable'      => true, // beta
				),
				// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
				'fields'      => array(
					array(
						'name' => 'Name',
						'id'   => 'contributor_name',
						'type' => 'text',
						// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
					),
					array(
						'name' => 'Description',
						'description' => 'Write a short description for this contributor',
						'id'   => 'contributor_description',
						'type' => 'textarea_small',
					),
					array(
						'name'  => 'Profile Picture',
						'id'    => 'contributor_image',
						'type'  => 'file',
						'allow' => 'attachment'
					),
					array(
						'name' => 'Website',
						'id'   => 'contributor_website',
						'type' => 'text_url',
					),
					array(
						'name' => 'Twitter',
						'id'   => 'contributor_twitter',
						'type' => 'text',
					)
				),
			),
		),
	);

	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once get_template_directory() . '/inc/metabox/init.php';

}
