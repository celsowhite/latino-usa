<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

<!-- Favicon -->

<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri().'/images/favicon.png'; ?>" />

<?php wp_head(); ?>

<!-- Google Analytics -->

<?php if (get_field('lusa_google_analytics', 'option')): ?>

	<script>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php the_field('lusa_google_analytics', 'option'); ?>']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

<?php endif; ?>

</head>

<body id="page-top" <?php body_class(); ?>>
  <div id="page" class="hfeed site">
  	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', '_s'); ?></a>

		<header id="masthead" class="site-header navbar-fixed--top sticky-nav" role="banner">
			<nav class="container flex navbar" role="navigation">
				<div class="navbar-header">
					<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">
						<img src="<?php echo get_template_directory_uri() . '/images/npr_latinoUsa_logo_50.png'; ?>">
					</a>
				</div>
				<!-- /.navbar-header -->
				<div class="flex-item"></div>
				<div class="navbar-right">
					<div class="row--no-gutter end-xs primary-nav">
						<button type="button" name="button" class="mobile-nav__toggle">
							<i class="fa fa-bars" style="color:#ccc;"></i>
						</button>
						<div class="navbar-main-collapse">
							<ul class="nav navbar-nav">
								<li class="hidden active">
									<a href="#page-top"></a>
								</li>
								<?php
									$links = get_field('lusa_specialepisode_navigation_links');
									$counter = 0;
									if($links)
									foreach ($links as $link) {
										$linkText = $link["link_text"];
										$linkUrl = $link["link_url"];
										?>
										<li>
											<a href="<?= $linkUrl; ?>" target="_blank"><?= $linkText; ?></a>
										</li>
										<?php
									}
									?>
									<?php echo do_shortcode('[cresta-social-share]'); ?>
								</ul>

						</div><!-- /.collapse -->
					</div>
				</div>
				<!-- /.navbar-collapse -->
			</nav>
  	</header>

    <div id="content" class="site-content">
