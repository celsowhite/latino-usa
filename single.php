
<?php 
	// Get the tags for this specific post
	$postID = get_queried_object()->ID;
	$tags = get_the_tags($postID);
	// First check if there are tags. 
	// If tags then Create an array of just the tag slugs.
	if($tags){
		$slugs = wp_list_pluck($tags, 'slug');
	}
	// Else set slugs to an empty array
	else {
		$slugs = [];
	}
	// Check if this post has tags and is a focus post. Then render the focus format
	if($tags && in_array('focus', $slugs)):
?>

<?php
/*
Template Name: Special Episodes
*/

get_header('special_episodes'); ?>

	<?php get_template_part('template-parts/focus', 'post'); ?>

<?php 
// Else a standard post and render our standard single.php format
else: ?>

<?php
/**
 * The template for displaying all single posts.
 *
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="lusa_grid">

				<div class="main_column_left">

					<div class="white_container">

						<header class="single_post_header">

							<div class="single_post_information">

								<!-- Post information (title, excerpt, reporters, categories, date) -->

								<div class="single_post_meta">

									<h2 class="blue"><?php the_title(); ?></h2>

									<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

									<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

									<?php the_category(); ?>

								</div>

								<?php if(get_field('lusa_excerpt')): ?>

									<p><?php the_field('lusa_excerpt'); ?></p>

								<?php endif; ?>

								<div class="single_post_share_icons_mobile">

									<h3>Share</h3>

									<?php echo do_shortcode('[cresta-social-share]'); ?>

								</div>

							</div>

							<!-- Legacy soundcloud audio embeds pulled from old custom field -->

							<?php $lusaembed = get_field('lusa_audio_embed'); if (empty($lusaembed)): ?>

								<?php if (get_post_meta($post->ID, 'Main Soundcloud', true)): ?>
									
									<?php echo (do_shortcode (get_post_meta ($post->ID, 'Main Soundcloud', true ))); ?>

								<?php endif; ?>

							<?php endif; ?>

						</header>

						<div class="single_post_main">

							<div class="single_post_share">

								<div class="single_post_share_icons regular">

									<h3>Share</h3>

									<?php echo do_shortcode('[cresta-social-share]'); ?>

								</div>

							</div>

							<article class="single_post_content wysiwyg">

								<!-- Featured Media -->

								<?php $post_format = get_post_format(); ?>

								<!-- Video -->

								<?php if($post_format == 'video'): ?>

									<?php the_field('lusa_video_embed'); ?>

								<!-- Image (w/ Caption and without) -->

								<?php else: ?>

									<?php if (get_field('lusa_featured_image_caption')): ?>
										
										<div class="featured_image_with_caption">
											<?php the_post_thumbnail('full'); ?>
											<figcaption class="wp-caption-text"><?php the_field('lusa_featured_image_caption'); ?></figcaption>
										</div>

									<?php else: ?>

										<?php the_post_thumbnail('full'); ?>

									<?php endif; ?>

								<?php endif; ?>

								<?php the_content(); ?>

							</article>

						</div>

					</div>

					<!-- Manual Related Posts. New method of adding related posts created in 2018 when we did the transition from episode style to single audio. -->

					<?php $related_posts = get_field('related_posts'); if($related_posts): ?>

						<p class="inpage_header">Related posts:</p>
						
						<ul class="single_post_related_segments">

							<?php foreach( $related_posts as $post ): ?>

								<?php setup_postdata($post); ?>

								<!-- Related Post Information -->

								<li>
									<a href="<?php the_permalink(); ?>" class="related_segment_content">
										<div class="image_container">
											<?php the_post_thumbnail(); ?>
										</div>
										<p><?php the_title(); ?></p>
										<span class="read_more">Read More</span>
									</a>
								</li>

							<?php endforeach; ?>

						</ul>

					<?php wp_reset_postdata(); endif; ?>

					<!-- If apart of an episode then list all segments from that episode. -->

					<?php 
					$related_segments_args = array(
						'post_type' => array('lusa_episode'),
						'order' => 'ASC',
						'orderby' => 'menu_order',
						'meta_query' => array(
							array(
								'key' => 'related_segments',
								'value' => '"' . get_the_ID() . '"',
								'compare' => 'LIKE'
							)
						)
					);
					$related_segments_loop = new WP_Query($related_segments_args);
					if ( $related_segments_loop->have_posts() ): while ( $related_segments_loop->have_posts() ): $related_segments_loop -> the_post();
					?>

						<div class="blue_long_cta margin_bottom_20">
							<a href="<?php the_permalink(); ?>">
								<span>View all of <?php the_title(); ?></span>
								<i class="fa fa-arrow-circle-o-right"></i>
							</a>
						</div>

						<ul class="single_post_related_segments">

							<?php $posts = get_field('related_segments'); if($posts): foreach($posts as $post): setup_postdata($post); ?>

								<li>
									<a href="<?php the_permalink(); ?>" class="related_segment_content">
										<div class="image_container">
											<?php the_post_thumbnail(); ?>
										</div>
										<p><?php the_title(); ?></p>
										<span class="read_more">Read More</span>
									</a>
								</li>

							<?php endforeach; wp_reset_postdata(); endif; ?>

						</ul>

					<?php endwhile; wp_reset_postdata(); endif; ?>

					<!-- If an old segment (pre 2015) then show other related segments via the legacy tagging system. -->
					
					<?php
						$segments = wp_get_post_terms($post->ID, 'segment', array("fields" => "slugs"));
						$segments = array_filter($segments, function($value) {
							return $value != 'sticky';
						});
						$segments = array_values($segments);
						if($segments):
					?>

						<?php $segment = $segments[0]; ?>

						<?php $excludeId = $post->ID; ?>

						<ul class="single_post_related_segments">

							<?php 

							$args3 = array('post_type'=>'post', 'segment' => $segment, 'post__not_in' => array($excludeId), 'posts_per_page' => 6 );
							$the_query3 = new WP_Query( $args3 ); 
							if ( $the_query3->have_posts() && $related_segments_loop->have_posts() == false ) : while ( $the_query3->have_posts() ) : $the_query3->the_post();

							?>

								<li>
									<a href="<?php the_permalink(); ?>" class="related_segment_content">
										<div class="image_container">
											<?php the_post_thumbnail(); ?>
										</div>
										<p><?php the_title(); ?></p>
										<span class="read_more">Read More</span>
									</a>
								</li>

							<?php endwhile; wp_reset_postdata(); endif; ?>

						</ul>

					<?php endif; ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					?>

				</div>

				<div class="sidebar_right">

					<!-- Spotify Playlist -->

					<?php if (get_field('spotify_playlist_embed')): ?>

						<div class="widget widget_area">

							<h3 class="title">The Playlist</h3>

							<div class="spotify_playlist">
								<?php the_field('spotify_playlist_embed'); ?>
							</div>

						</div>

					<?php endif; ?>

					<!-- Single Post Sidebar -->

					<?php dynamic_sidebar('lusa_sidebar'); ?>

				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php endif; ?>

<?php get_footer(); ?>
