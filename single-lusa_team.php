<?php
/**
 * The template for displaying all single reporter pages with posts from that reporter.
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Reporter Information -->

			<div class="lusa_grid">

				<div class="image_column">

					<a class="team_member_image" href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('thumbnail'); ?>
					</a>

				</div>

				<div class="details_column">

					<div class="reporter_information wysiwyg">

						<h2><?php the_title(); ?></h2>
						<ul class="team_social_list">
							<?php if (get_field('team_website')): ?>
								<li>
									<a href="<?php the_field('reporter_website'); ?>" target="_blank">
										<i class="fa fa-laptop"></i>
									</a>
								</li>
							<?php endif; ?>
							<?php if (get_field('team_twitter')): ?>
								<li>
									<a href="https://twitter.com/<?php the_field('reporter_twitter'); ?>" target="_blank">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
							<?php endif; ?>
							<?php if (get_field('team_facebook')): ?>
							<li>
								<a href="https://facebook.com/<?php the_field('reporter_facebook'); ?>" target="_blank">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<?php endif; ?>
							<?php if (get_field('team_instagram')): ?>
							<li>
								<a href="https://instagram.com/<?php the_field('reporter_instagram'); ?>" target="_blank">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
							<?php endif; ?> 
							<?php if (get_field('team_linkedin')): ?>
							<li>
								<a href="https://www.linkedin.com/in/<?php the_field('reporter_linkedin'); ?>" target="_blank">
									<i class="fa fa-linkedin"></i>
								</a>
							</li>
							<?php endif; ?> 
						</ul>
						<?php the_content(); ?>

					</div>

				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
