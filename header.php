<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/images/favicon.png'; ?>" />

<?php wp_head(); ?>

<!-- Google Analytics -->

<?php if(get_field('lusa_google_analytics','option')): ?>

	<script>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php the_field('lusa_google_analytics','option'); ?>']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

<?php endif; ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<?php if (get_field('header_banner_image','option')): ?>
		
			<!-- Optional Header Banner -->

			<div class="header_banner">
				<a href="<?php the_field('header_banner_link', 'option'); ?>" target="_blank">
					<img src="<?php the_field('header_banner_image', 'option'); ?>" />
				</a>
			</div>

		<?php else: ?>

			<!-- Header top with logo, tagline and donate cta -->

			<div class="header_top">

				<div class="logo_tagline_container">

					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php the_field('lusa_logo','option'); ?>" />
					</a>

					<p>Part of the Futuro Media Group</p>

				</div>

				<a href="<?php the_field('donate_link','option'); ?>" class="blue_cta">Donate</a>

			</div>

		<?php endif; ?>

		<!-- Header bottom with navigation, search and social links -->

		<div class="header_bottom">

			<?php wp_nav_menu( array( 'theme_location' => 'header', 'menu_id' => 'header-menu', 'container' => false ) ); ?>

			<ul class="mobile_visibile_menu">
				<?php 
					// Save the mobile header menu items array
					$mobile_header_menu_array = wp_get_nav_menu_items('mobile-header-menu'); 
					// Loop through each item and group the li outputs together
					$menu_list = '';
					foreach ( $mobile_header_menu_array as $menu_item ) {
						$title = $menu_item->title;
						$url = $menu_item->url;
						$target = $menu_item->target;
						$menu_list .= '<li><a href="' . $url . '" target="' . $target . '">' . $title . '</a></li>';
					}
					// Output the dynamic menu items
					echo $menu_list;
				?>
				<li><a href="" class="mobile_menu_link">Menu</a></li>
				<li>
					<span class="search_link">
						<i class="fa fa-lg fa-search"></i>
					</span>
				</li>

			</ul>

			<ul class="header_icons">
				<li>
					<a href="https://twitter.com/LatinoUSA" target="_blank"><i class="fa fa-lg fa-twitter"></i></a>
				</li>
				<li>
					<a href="https://www.facebook.com/latinousa/" target="_blank"><i class="fa fa-lg fa-facebook"></i></li></a>
				</li>
				<li>
					<a href="https://instagram.com/latinousa/" target="_blank"><i class="fa fa-lg fa-instagram"></i></a>
				</li>
				<li>
					<span class="search_link">
						<i class="fa fa-lg fa-search"></i>
					</span>
				</li>
			</ul>

		</div>

	</header>

	<?php get_template_part('template-parts/header', 'topics'); ?>

	<div class="search_drawer">

		<?php get_search_form(); ?>

	</div>

	<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_id' => 'mobile_hidden_menu', 'container' => false ) ); ?>

	<div id="content" class="site-content">
