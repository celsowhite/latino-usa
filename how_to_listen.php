<?php
/*
Template Name: How To Listen
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

	<main id="main" class="main_wrapper" role="main">

		<p class="inpage_header">How To Listen</p>

		<div class="lusa_grid">

			<div class="main_column_left">

				<div class="white_container">

					<ul class="htl_grid">

						<?php if(have_rows('htl_method')): while(have_rows('htl_method')): the_row(); ?>

							<li>
								<a href="<?php the_sub_field('htl_method_link'); ?>" target="_blank">
									<div class="logo">
										<img src="<?php the_sub_field('htl_method_image'); ?>" />
									</div>
								</a>
							</li>

						<?php endwhile; endif; ?>

					</ul>

				</div>

			</div>

			<div class="sidebar_right">

				<?php dynamic_sidebar('lusa_sidebar'); ?>

			</div>

		</div>

	</main>

	<?php endwhile;?>

<?php get_footer(); ?>
