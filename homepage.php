<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php if(get_the_content() == ''): ?>

				<p class="inpage_header"><?php the_field('audio_title'); ?></p>

			<?php endif; ?>

			<div class="lusa_grid">

				<div class="main_column_left">

					<?php 
					// If the homepage content area has content in it then don't show the latest podcast.
					if(get_the_content() != ''): ?>

						<?php the_content(); ?>

					<?php else: ?>

						<!-- Latest Podcast -->
					
						<?php
							$latest_podcast_args = array('post_type' => 'post', 'posts_per_page' => 1, 'order' => 'DSC', 'order_by' => 'date', 'tax_query' => array (
									array('taxonomy' => 'post_format', 'field' => 'slug', 'terms' => array('post-format-audio'), 'operator' => 'IN')
								),
							);
							$latest_podcast_loop = new WP_Query($latest_podcast_args);
							if ( $latest_podcast_loop->have_posts() ) : while ( $latest_podcast_loop->have_posts() ) : $latest_podcast_loop->the_post();
						?>
							<div class="post_container">

								<div class="top_content">
								
									<div class="text">

										<div class="title">

											<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

											<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

											<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

										</div>

										<?php if(get_field('lusa_excerpt')): ?>

											<p><?php the_field('lusa_excerpt'); ?></p>

										<?php endif; ?>

									</div>
									
									<div class="media">

										<!-- Featured Image or Video -->

										<?php $post_format = get_post_format(); ?>

										<?php if ($post_format == 'video'): ?>

											<?php the_field('lusa_video_embed'); ?>

										<?php else: ?>

											<a href="<?php the_permalink(); ?>">
												<?php the_post_thumbnail(); ?>
											</a>

										<?php endif; ?>

									</div>

								</div>

								<?php the_field('lusa_audio_embed'); ?>

							</div>

						<?php endwhile; wp_reset_postdata(); endif; ?>

						<div class="blue_long_cta">
							<a href="<?php the_field('audio_link_url'); ?>">
								<span><?php the_field('audio_link_title'); ?></span>
								<i class="fa fa-arrow-circle-o-right"></i>
							</a>
						</div>

					<?php endif; ?>

					<!-- Homepage News Feed -->

					<p class="inpage_header" style="margin-top: 20px;"><?php the_field('news_title'); ?></p>

					<?php get_template_part('template-parts/homepage_featured_post'); ?>

					<div class="blue_long_cta">

						<a href="<?php the_field('news_link_url'); ?>">

							<span><?php the_field('news_link_title'); ?></span>
							<i class="fa fa-arrow-circle-o-right"></i>
							
						</a>

					</div>

				</div>

				<div class="sidebar_right mobile_visible">
					
					<?php dynamic_sidebar('homepage_sidebar'); ?>

				</div>

			</div>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
