<?php
/**
 * The template for displaying all single episodes.
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="lusa_grid">

				<div class="main_column_left">

					<!-- Episode Information -->

					<div class="post_container">

						<div class="top_content">
							
							<div class="text">

								<div class="title">

									<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

									<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

									<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

								</div>

								<?php if(get_field('lusa_excerpt')): ?>

									<p><?php the_field('lusa_excerpt'); ?></p>

								<?php endif; ?>

								<div class="single_episode_share_icons_mobile">

									<?php echo do_shortcode('[cresta-social-share]'); ?>

								</div>

							</div>
							
							<div class="media">

								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail(); ?>
								</a>

							</div>

						</div>

						<?php the_field('lusa_audio_embed'); ?>

					</div>

					<?php if(get_the_content()): ?>
						
						<div class="white_container wysiwyg">

							<?php the_content(); ?>

						</div>

					<?php endif; ?>

					<!-- Segments -->

					<?php $posts = get_field('related_segments'); if($posts): ?>

						<p class="inpage_header">Stories from <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>

						<div class="white_container">

							<?php foreach( $posts as $post ): ?>

								<?php setup_postdata($post); ?>

								<!-- Segment information -->

								<div class="segment_container">
						        	<div class="segment_info">

							            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

							            <a class="read_more" href="<?php the_permalink(); ?>">
							            	<span>Read More</span>
							            	<i class="fa fa-arrow-circle-o-right"></i>
							        	</a>

							        </div>
							        <div class="segment_embed">
							            <?php the_field('lusa_audio_embed'); ?>
							        </div>
						        </div>

							<?php endforeach; ?>

						</div>

					<?php wp_reset_postdata(); ?>

					<?php endif; ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					?>

				</div>

				<div class="sidebar_right">

					<!-- Share This Episode -->

					<div class="widget widget_area">

						<h3 class="title">Share <?php the_title(); ?></h3>

						<?php echo do_shortcode('[cresta-social-share]'); ?>

					</div>

					<!-- Social Question -->
					
					<?php if (get_field('lusa_social_question_text')): ?>

						<div class="widget widget_area social_widget">

							<h3 class="title"><?php the_field('lusa_social_question_title'); ?></h3>

							<p><?php the_field('lusa_social_question_text'); ?></p>

							<a class='twitter_share blue_cta' href='<?php the_permalink(); ?>' text="<?php the_title(); ?>" via='latinousa'>Tweet Us <i class='fa fa-lg fa-twitter'></i></a>

						</div>

					<?php endif; ?>

					<!-- Spotify Playlist -->

					<?php if (get_field('spotify_playlist_embed')): ?>

						<div class="widget widget_area">

							<h3 class="title">The Playlist</h3>

							<div class="spotify_playlist">
								<?php the_field('spotify_playlist_embed'); ?>
							</div>

						</div>

					<?php endif; ?>
					
					<?php dynamic_sidebar('single_episode_sidebar'); ?>
				
				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>