<?php
/**
 * The template for displaying all single reporter pages with posts from that reporter.
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Reporter Information -->

			<div class="lusa_grid">

				<div class="image_column">

					<a class="team_member_image" href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('thumbnail'); ?>
					</a>

				</div>

				<div class="details_column">

					<div class="reporter_information wysiwyg">

						<h2><?php the_title(); ?></h2>
						<ul class="team_social_list">
							<?php if (get_field('reporter_website')): ?>
								<li>
									<a href="<?php the_field('reporter_website'); ?>" target="_blank">
										<i class="fa fa-laptop"></i>
									</a>
								</li>
							<?php endif; ?>
							<?php if (get_field('reporter_twitter')): ?>
								<li>
									<a href="https://twitter.com/<?php the_field('reporter_twitter'); ?>" target="_blank">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
							<?php endif; ?>
							<?php if (get_field('reporter_facebook')): ?>
							<li>
								<a href="https://facebook.com/<?php the_field('reporter_facebook'); ?>" target="_blank">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<?php endif; ?>
							<?php if (get_field('reporter_instagram')): ?>
							<li>
								<a href="https://instagram.com/<?php the_field('reporter_instagram'); ?>" target="_blank">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
							<?php endif; ?> 
							<?php if (get_field('reporter_linkedin')): ?>
							<li>
								<a href="https://www.linkedin.com/in/<?php the_field('reporter_linkedin'); ?>" target="_blank">
									<i class="fa fa-linkedin"></i>
								</a>
							</li>
							<?php endif; ?> 
						</ul>
						<?php the_content(); ?>

					</div>

				</div>

			</div>

			<div class="lusa_grid">

				<p class="inpage_header">Stories by <?php the_title(); ?></p>

				<div class="main_column_left">

					<!-- Reporter Episodes/Posts -->

					<?php 
					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
					$reporter_posts_args = array(
						'post_type' => array('lusa_episode', 'post', 'page'),
						'order' => 'DSC',
						'orderby' => 'date',
						'posts_per_page' => 5,
						'paged' => $paged,
						'meta_query' => array(
							array(
								'key' => 'lusa_reporters',
								'value' => '"' . get_the_ID() . '"',
								'compare' => 'LIKE'
							)
						)
					);
					$reporter_posts_loop = new WP_Query($reporter_posts_args);
					if ( $reporter_posts_loop->have_posts() ): while ( $reporter_posts_loop->have_posts() ): $reporter_posts_loop -> the_post();
					?>

						<div class="post_container">

							<div class="top_content">

								<!-- Title, Reporters & Excerpt -->

								<div class="text">

									<div class="title">

										<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

										<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

										<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

									</div>

									<?php if(get_field('lusa_excerpt')): ?>

										<p><?php the_field('lusa_excerpt'); ?></p>

									<?php endif; ?>

								</div>

								<!-- Featured Image or Video -->

								<div class="media">

									<?php $post_format = get_post_format(); ?>

									<?php if ($post_format == 'video'): ?>

											<?php the_field('lusa_video_embed'); ?>

									<?php else: ?>

											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

									<?php endif; ?>

								</div>

							</div>

							<!-- Audio Embed -->

							<?php the_field('lusa_audio_embed'); ?>

						</div>

					<?php endwhile; ?>

					<div class="lusa_pagination">

						<span class="previous_posts">
							<?php echo custom_pagination_link( '<i class="fa fa-arrow-circle-o-left"></i> Newer', 'prev', $reporter_posts_loop ); ?>
						</span>

						<span class="next_posts">
							<?php echo custom_pagination_link( 'Older <i class="fa fa-arrow-circle-o-right"></i>', 'next', $reporter_posts_loop ); ?>
						</span>

					</div>

					<?php else: ?>
					
					<p></p>

					<?php wp_reset_postdata(); endif;?>

				</div>

				<div class="sidebar_right">

					<?php dynamic_sidebar('lusa_sidebar'); ?>

				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
