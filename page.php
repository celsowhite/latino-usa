<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

	<?php if (get_field('header_image')): ?>

		<div class="header_image">

			<img src="<?php the_field('header_image'); ?>" />

			<?php if(get_field('header_image_text')): ?>
				<div class="title animated fadeInUp">
					<p><?php the_field('header_image_text'); ?></p>
				</div>
			<?php endif; ?>

		</div>
		
	<?php endif; ?>

	<main id="main" class="main_wrapper" role="main">

		<div class="lusa_grid">

			<div class="main_column_left">

				<div class="white_container wysiwyg">

					<h2 class="blue"><?php the_title(); ?></h2>

					<?php the_content(); ?>

				</div>

			</div>

			<div class="sidebar_right">

				<?php if(have_rows('lusa_custom_widgets')): while(have_rows('lusa_custom_widgets')): the_row(); ?>

					<div class="widget_area">

						<h3 class="title"><?php the_sub_field('lusa_custom_widget_title'); ?></h3>

						<?php the_sub_field('lusa_custom_widget_content'); ?>

					</div>

				<?php endwhile; endif; ?>

				<?php dynamic_sidebar('lusa_sidebar'); ?>

			</div>

		</div>

	</main>

	<?php endwhile;?>

<?php get_footer(); ?>
