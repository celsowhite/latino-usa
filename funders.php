<?php
/*
Template Name: Funders
*/

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

			<div class="lusa_grid">

				<div class="main_column_left">

					<div class="white_container">

						<ul class="htl_grid">

							<?php
								$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
								$funders_args = array('post_type' => 'lusa_funder', 'order' => 'ASC', 'orderby' => 'menu_order', 'posts_per_page' => 50, 'paged' => $paged );
								$funders_loop = new WP_Query($funders_args);
								if ( $funders_loop->have_posts() ) : while ( $funders_loop->have_posts() ) : $funders_loop->the_post();
							?>

								<li>

									<?php if(get_field('funder_stories_question')): ?>

										<a href="<?php the_permalink(); ?>">

											<div class="logo">
												<?php the_post_thumbnail('thumbnail'); ?>
											</div>

										</a>

									<?php elseif(empty(get_field('funder_stories_question')) && get_field('funder_website')): ?>

										<a href="<?php the_field('funder_website'); ?>" target="_blank">

											<div class="logo">
												<?php the_post_thumbnail('thumbnail'); ?>
											</div>

										</a>

									<?php else: ?>

										<div class="logo">
											<?php the_post_thumbnail('thumbnail'); ?>
										</div>

									<?php endif; ?>

									<div class="funder_links">
										<?php if(get_field('funder_website')): ?>

											<a href="<?php the_field('funder_website'); ?>" target="_blank">Website</a>
											
										<?php endif; ?>

										<?php if(get_field('funder_stories_question')): ?>

											• <a class="funder_links" href="<?php the_permalink(); ?>">View Stories</a>

										<?php endif; ?>
									</div>

								</li>

							<?php endwhile; ?>

						</ul>

						<div class="lusa_pagination">

							<span class="previous_posts">
								<?php echo get_previous_posts_link('<i class="fa fa-arrow-circle-o-left"></i> Previous'); ?>
							</span>

							<span class="next_posts">
								<?php echo get_next_posts_link('More <i class="fa fa-arrow-circle-o-right"></i>', $funders_loop->max_num_pages); ?>
							</span>

						</div>

						<?php wp_reset_postdata(); endif; ?>

					</div>

				</div>

				<div class="sidebar_right">

					<?php if(have_rows('lusa_custom_widgets')): while(have_rows('lusa_custom_widgets')): the_row(); ?>

						<div class="widget_area">

							<h3 class="title"><?php the_sub_field('lusa_custom_widget_title'); ?></h3>

							<?php the_sub_field('lusa_custom_widget_content'); ?>

						</div>

					<?php endwhile; endif; ?>
					
					<?php dynamic_sidebar('lusa_sidebar'); ?>

				</div>

			</div>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>