<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	</div>

	<footer class="site_footer">

		<div class="footer_top">

			<div class="footer_container">

				<p><?php the_field('lusa_footer_text', 'option'); ?></p>

				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu', 'container' => false ) ); ?>

				<div id="mc_embed_signup">
					<p>Sign up for monthly updates from the Latino USA team</p>
					<form action="//futuromediagroup.us2.list-manage.com/subscribe/post?u=4a536459ac050fe60bfcdddd7&amp;id=636c56063c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					    <div id="mc_embed_signup_scroll">
							<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your Email" required>
						    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;"><input type="text" name="b_bfaeb9f9cfe94bf88ebe82105_58d7ecad1c" tabindex="-1" value=""></div>
						    <input type="submit" value="Join" name="subscribe" id="mc-embedded-subscribe" class="button">
					    </div>
					</form>
				</div>

			</div>

		</div>

		<div class="footer_bottom">

			<div class="footer_container">

				<ul>
					<?php wp_nav_menu( array( 'menu' => 'media-properties', 'menu_id' => 'media-properties', 'container' => false ) ); ?>
				</ul>

				<?php if(get_field('lusa_footer_address', 'option')): ?>
					<p><?php the_field('lusa_footer_address', 'option'); ?></p>
				<?php endif; ?>
			
			</div>

		</div>

	</footer>
	
</div>

<?php wp_footer(); ?>

</body>
</html>
