#Latino USA 2015

Development of a new theme for latinousa.org. Based on _s theme by Wordpress.

##Instructions

* Install the latest version of Wordpress locally.
* Setup your wp-config file.
* Login to the Latino USA server via FTP or SSH and download the entire wp-content folder excluding the 'latinousa' theme from wp-content/themes.
* Upload the wp-content folder to your local setup.
* Git clone this repository into a folder 'latinousa' within wp-content/themes.
* Download the current database dump eigther using phpMyAdmin or WP-CLI db export.
* Import the database to your local sql.
* Do a search and replace of the database to change URL strings to your local URL.