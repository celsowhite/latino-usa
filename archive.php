<?php
/**
 * General archive template for displaying posts.
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php if ( have_posts() ): ?>

		<div class="lusa_grid">

			<p class="inpage_header"><?php single_cat_title(); ?></p>

			<div class="main_column_left">

				<?php $post_count=0; while ( have_posts() ) : the_post(); ?>
					
				 	<?php if($post_count==0): ?>

						<div class="post_container_large">

							<div class="top_title">

								<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

							</div>

							<!-- Featured Image or Video -->

							<div class="media">

								<?php $post_format = get_post_format(); ?>

								<?php if ($post_format == 'video'): ?>

									<?php the_field('lusa_video_embed'); ?>

								<?php else: ?>

									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('full'); ?>
									</a>

								<?php endif; ?>

							</div>

							<div class="text">

								<div class="title">

									<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

									<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

								</div>

								<?php if(get_field('lusa_excerpt')): ?>

									<p><?php the_field('lusa_excerpt'); ?></p>

								<?php endif; ?>

								<a class="read_more" href="<?php the_permalink(); ?>">
					            	<span>Read More</span>
					            	<i class="fa fa-arrow-circle-o-right"></i>
					        	</a>

							</div>

						</div>

					<?php else: ?>

						<div class="post_container">

							<!-- Title, Reporters & Excerpt -->

							<div class="text">

								<div class="title">

									<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

									<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

									<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

								</div>

								<?php if(get_field('lusa_excerpt')): ?>

									<p><?php the_field('lusa_excerpt'); ?></p>

								<?php endif; ?>

								<a class="read_more" href="<?php the_permalink(); ?>">
					            	<span>Read More</span>
					            	<i class="fa fa-arrow-circle-o-right"></i>
					        	</a>

							</div>

							<!-- Featured Image or Video -->

							<div class="media">

								<?php $post_format = get_post_format(); ?>

								<?php if ($post_format == 'video'): ?>

									<?php the_field('lusa_video_embed'); ?>

								<?php else: ?>

									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>

								<?php endif; ?>

							</div>

						</div>

					<?php endif; ?>

				<?php $post_count++; endwhile;?>

				<div class="lusa_pagination">

					<span class="previous_posts">
						<?php echo get_previous_posts_link('<i class="fa fa-arrow-circle-o-left"></i> Newer'); ?>
					</span>

					<span class="next_posts">
						<?php echo get_next_posts_link('Older <i class="fa fa-arrow-circle-o-right"></i>', 0); ?>
					</span>

				</div>

			</div>

			<div class="sidebar_right">

				<?php dynamic_sidebar('lusa_sidebar'); ?>

			</div>

		</div>

		<?php endif; ?>

	</main>

<?php get_footer(); ?>