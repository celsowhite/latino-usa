<?php
/*
Template Name: Submit
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

	<div class="header_image">

		<img src="<?php the_field('header_image'); ?>" />

		<?php if(get_field('header_image_text')): ?>
			<div class="title animated fadeInUp">
				<p><?php the_field('header_image_text'); ?></p>
			</div>
		<?php endif; ?>

	</div>

	<main id="main" class="main_wrapper" role="main">

		<div class="lusa_grid">

			<div class="sidebar_left">

				<?php if(have_rows('lusa_custom_widgets')): while(have_rows('lusa_custom_widgets')): the_row(); ?>

					<div class="widget_area">

						<h3 class="title"><?php the_sub_field('lusa_custom_widget_title'); ?></h3>

						<?php the_sub_field('lusa_custom_widget_content'); ?>

					</div>

				<?php endwhile; endif; ?>

			</div>

			<div class="main_column_right">

				<div class="white_container">

					<?php the_content(); ?>

					<?php gravity_form( 2, $display_title = true, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $echo = true ); ?>

				</div>

			</div>

		</div>

	</main>

	<?php endwhile;?>

<?php get_footer(); ?>
