<?php
/*
Template Name: Reporters
*/

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<p class="inpage_header">Reporters</p>

			<div class="lusa_grid">

				<div class="main_column_left">

					<ul class="team_grid">
					
						<?php

						// Loop through all reporters except Latino USA team members
						
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$reporters_args = array('post_type' => 'lusa_reporter', 'posts_per_page' => 16, 'order' => 'ASC', 'orderby' => 'title', 'paged' => $paged,
							'tax_query' => array(array('taxonomy'=>'lusa_reporter_category', 'field' => 'slug', 'terms' => array('team'), 'operator' => 'NOT IN' )),
						);
						$reporters_loop = new WP_Query($reporters_args);
						if ( $reporters_loop->have_posts() ) : while ( $reporters_loop->have_posts() ) : $reporters_loop->the_post();
						?>

							<li>
								<a class="team_member_image" href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('thumbnail'); ?>
								</a>
								<h3 class="name"><?php the_title(); ?></h3>
							</li>

						<?php endwhile; ?>

					</ul>

						<div class="lusa_pagination">

							<span class="previous_posts">
								<?php echo get_previous_posts_link('<i class="fa fa-arrow-circle-o-left"></i> Previous'); ?>
							</span>

							<span class="next_posts">
								<?php echo get_next_posts_link('More <i class="fa fa-arrow-circle-o-right"></i>', $reporters_loop->max_num_pages); ?>
							</span>

						</div>

						<?php wp_reset_postdata(); endif; ?>

				</div>

				<div class="sidebar_right">
					
					<?php dynamic_sidebar('lusa_sidebar'); ?>

				</div>

			</div>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
