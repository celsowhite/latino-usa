<?php
/*
Template Name: About
*/

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

	<div class="header_image">

		<img src="<?php the_field('header_image'); ?>" />

		<?php if (get_field('header_image_text')): ?>
			<div class="title animated fadeInUp">
				<?php the_field('header_image_text'); ?>
			</div>
		<?php endif; ?>

	</div>

	<main id="main" class="main_wrapper" role="main">

		<div class="lusa_grid">

			<div class="main_column_left">

				<div class="white_container wysiwyg">

					<?php the_content(); ?>

				</div>

			</div>

			<div class="sidebar_right">

				<?php if(have_rows('lusa_custom_widgets')): while(have_rows('lusa_custom_widgets')): the_row(); ?>

					<div class="widget_area">

						<h3 class="title"><?php the_sub_field('lusa_custom_widget_title'); ?></h3>

						<?php the_sub_field('lusa_custom_widget_content'); ?>

					</div>

				<?php endwhile; endif; ?>

			</div>

		</div>

		<h2 class="blue">Our Production Team</h2>

		<ul class="team_grid">

			<?php
				$production_team_args = array('post_type' => 'lusa_team', 'lusa_team_category' => 'production', 'posts_per_page' => -1);
				$production_team_loop = new WP_Query($production_team_args);
				if ( $production_team_loop->have_posts() ) : while ( $production_team_loop->have_posts() ) : $production_team_loop->the_post();
			?>
				<li>
					<div class="team_member_image team_popup" href="#<?php the_ID(); ?>">
						<?php the_post_thumbnail('thumbnail'); ?>
						<h3 class="name"><?php the_title(); ?></h3>
					</div>
				</li>

				<!-- Popup With Bio -->

				<div id="<?php the_ID(); ?>" class="white_popup mfp-hide">
					<div class="row">

						<!-- Image and Social -->

						<div class="medium-4 columns">
							<div class="team_member_image">
								<?php the_post_thumbnail('medium'); ?>
							</div>
							<ul class="team_social_list">
								<?php if (get_field('team_website')): ?>
									<li>
										<a href="<?php the_field('team_website'); ?>" target="_blank">
											<i class="fa fa-laptop"></i>
										</a>
									</li>
								<?php endif; ?> 
								<?php if (get_field('team_twitter')): ?>
									<li>
										<a href="https://twitter.com/<?php the_field('team_twitter'); ?>" target="_blank">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
								<?php endif; ?>
								<?php if (get_field('team_facebook')): ?>
									<li>
										<a href="https://facebook.com/<?php the_field('team_facebook'); ?>" target="_blank">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
								<?php endif; ?>
								<?php if (get_field('team_instagram')): ?>
									<li>
										<a href="https://instagram.com/<?php the_field('team_instagram'); ?>" target="_blank">
											<i class="fa fa-instagram"></i>
										</a>
									</li>
								<?php endif; ?> 
								<?php if (get_field('team_linkedin')): ?>
									<li>
										<a href="https://www.linkedin.com/in/<?php the_field('team_linkedin'); ?>" target="_blank">
											<i class="fa fa-linkedin"></i>
										</a>
									</li>
								<?php endif; ?> 
							</ul>
						</div>

						<!-- Name, Title & Bio -->

						<div class="medium-8 columns">

							<div class="team_member_header">
								<h2 class="blue"><?php the_title(); ?></h2>
								<p><?php the_field('team_role'); ?></p>
							</div>

							<?php the_content(); ?>

							<?php if (get_field('team_reporter_page')): ?>
								<a class="blue_cta" href="<?php the_field('team_reporter_page'); ?>">View Posts by <?php the_title(); ?></a>
							<?php endif; ?>

						</div>

					</div>
				</div>

			<?php endwhile; endif; ?>

		</ul>

		<h2 class="blue">Our Administrative Team</h2>

		<ul class="team_grid">

			<?php
				$administrative_team_args = array('post_type' => 'lusa_team', 'lusa_team_category' => 'administrative', 'posts_per_page' => -1);
				$administrative_team_loop = new WP_Query($administrative_team_args);
				if ( $administrative_team_loop->have_posts() ) : while ( $administrative_team_loop->have_posts() ) : $administrative_team_loop->the_post();
			?>
				<li>
					<div class="team_member_image team_popup" href="#<?php the_ID(); ?>">
						<?php the_post_thumbnail('thumbnail'); ?>
						<h3 class="name"><?php the_title(); ?></h3>
					</div>
				</li>

				<!-- Popup With Bio -->

				<div id="<?php the_ID(); ?>" class="white_popup mfp-hide">
					<div class="row">

						<!-- Image and Social -->

						<div class="medium-4 columns">
							<div class="team_member_image">
								<?php the_post_thumbnail('medium'); ?>
							</div>
							<ul class="team_social_list">
								<?php if (get_field('team_website')): ?>
									<li>
										<a href="<?php the_field('team_website'); ?>" target="_blank">
											<i class="fa fa-laptop"></i>
										</a>
									</li>
								<?php endif; ?> 
								<?php if (get_field('team_twitter')): ?>
									<li>
										<a href="https://twitter.com/<?php the_field('team_twitter'); ?>" target="_blank">
											<i class="fa fa-twitter"></i>
										</a>
									</li>
								<?php endif; ?>
								<?php if (get_field('team_facebook')): ?>
									<li>
										<a href="https://facebook.com/<?php the_field('team_facebook'); ?>" target="_blank">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
								<?php endif; ?>
								<?php if (get_field('team_instagram')): ?>
									<li>
										<a href="https://instagram.com/<?php the_field('team_instagram'); ?>" target="_blank">
											<i class="fa fa-instagram"></i>
										</a>
									</li>
								<?php endif; ?> 
								<?php if (get_field('team_linkedin')): ?>
									<li>
										<a href="https://www.linkedin.com/in/<?php the_field('team_linkedin'); ?>" target="_blank">
											<i class="fa fa-linkedin"></i>
										</a>
									</li>
								<?php endif; ?> 
							</ul>
						</div>

						<!-- Name, Title & Bio -->

						<div class="medium-8 columns">

							<div class="team_member_header">
								<h2 class="blue"><?php the_title(); ?></h2>
								<p><?php the_field('team_role'); ?></p>
							</div>

							<?php the_content(); ?>

							<?php if (get_field('team_reporter_page')): ?>
								<a class="blue_cta" href="<?php the_field('team_reporter_page'); ?>">View Posts by <?php the_title(); ?></a>
							<?php endif; ?>

						</div>

					</div>
				</div>

			<?php endwhile; endif; ?>

		</ul>

	</main>

	<?php endwhile;?>

<?php get_footer(); ?>
