<?php
/**
 * Template part for displaying focus page content in single.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

?>

<main id="main" class="special-episodes children--fill-height" role="main">
    <section class="intro-body flex" id="intro">
      <div class="bg-cover flex flex--center" style="background-image:url('<?= the_field('lusa_specialepisode_cover_image');?>')">
        <div class="bg-cover__tint bg-cover__tint--black"></div>
        <div class="col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-0 flex-item intro-body__content">
          <div class="text-align--center">
            <h1 class="episode-heading"><?php the_field('lusa_specialepisode_episode_title'); ?></h1>
          </div>
        </div>
      </div>
    </section>

    <?php if (get_field('lusa_audio_players')): ?>
      <section class="section--padded section--transparent" id="episode-media">
    <?php else : ?>
      <section class="section--thin-padded section--transparent">
    <? endif ?>
        <div class="container">
          <div class="row center-xs center-sm center-m">

            <div class="single_post_meta">

              <?php get_template_part( 'template-parts/reporter', 'loop' ); ?>
              <span class="post_date"><?php echo get_the_date('M j, Y'); ?></span><?php the_category(); ?>

            </div> <!-- /.row -->

            <div class="single_post_meta">
              <?php echo do_shortcode('[cresta-social-share]'); ?>
            </div>

          </div>

        <div class="row center-xs ">
          
          <?php 
          $players = get_field('lusa_audio_players');
          if($players):
          ?> 
            <?php foreach($players as $player) {
              ?>
              <div class="col-xs-12 col-sm-6">
                <iframe class="box-shadow--xl--up" src="<?= $player['audio_url'] ?>" width="100%" height="290" frameborder="0" scrolling="no" title="<?= $player['audio_title']?>"></iframe>
              </div>
            <?php } ?>

          <?php endif; ?>

        </div> <!-- /.row -->

      </div>
    </section>

    <section class="section--padded" id="episode-article">
      <div class="container">
        <div class="flex flex--center">
          <div class="col-xs-12 col-md-10 body-text">
            <?php if (have_posts()) : while (have_posts()) : the_post();?>
              <?php the_content(); ?>
              <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                  comments_template();
                endif;
              ?>
            <?php endwhile; endif; ?>
          </div>
        </div>
      </div>
    </section>

    <?php if( get_field('lusa_specialepisode_producer_credits')): ?>
    <section class="section--padded" id="episode-credits">
      <div class="container body-text">
        <header class="section-title text-align--center text-uppercase">
          <h3>Credits</h3>
          <hr/>
        </header>
        <?php if( get_field('lusa_specialepisode_producer_credits') ): ?>
        <div class="row center-xs">
          <div class="col-lg-6 col-md-8 col-sm-10">
            <p>
              Produced by <?= the_field('lusa_specialepisode_producer_credits')?>
            </p>
            <p>
              <?= the_field('lusa_specialepisode_producer_credits_summary')?>
            </p>
          </div>
        <?php endif ?>
        <?php if( get_field('lusa_specialepisode_logos') ): ?>
          <div class="clearfix"></div>
          <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
            <div class="row center-xs padding-top--2 text-align--center">
            <?php
              $logos = get_field('lusa_specialepisode_logos');
              $counter = 0;
              foreach ($logos as $logo) {
                $captionText = strip_tags($logo["logo_caption"]);
                $figcaptionTemplate = empty($logo["logo_caption"])
                  ? null
                  : '<figcaption class="no-border">'. $captionText . '</figcaption>';
                ?>
                  <div class="col-xs-12 col-sm">
                    <figure class="logo">
                      <?= $figcaptionTemplate ?>
                      <a class="display-block" href="<?= $logo['logo_link']; ?>" target="_blank">
                        <img
                          alt="<?= empty($captionText) ? 'logo image' : $captionText ?>"
                          src="<?= $logo['logo_image'] ?>">
                      </a>
                    </figure>
                  </div>
                <?php
                $counter++;
              }
              ?>
          </div> <!-- /.container -->
        <?php endif ?>
          </div>
        </div>
        <?php if( get_field('lusa_specialepisode_soundcloud_embed') ): ?>
        <div class="row center-xs">
          <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
            <?= the_field('lusa_specialepisode_soundcloud_embed'); ?>
          </div>
        </div>
        <?php endif ?>
      </div>
    </section>
  <?endif?>
  </main>