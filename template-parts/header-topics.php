<div class="topics_drawer">

    <ul>
        <li>
            <a href="<?php echo get_category_link(101); ?>" class="topic_header politics">Politics</a>

            <?php
                $politics_args = array('category_name' => 'politics', 'posts_per_page' => 1);
                $politics_loop = new WP_Query($politics_args);
                if ( $politics_loop->have_posts() ) : while ( $politics_loop->have_posts() ) : $politics_loop->the_post();
            ?>
                <a class="recent_post" href="<?php the_permalink(); ?>">
                    <div class="image_container">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <p><?php the_title(); ?></p>
                </a>
            <?php endwhile; wp_reset_postdata(); endif; ?>

            <a class="read_more" href="<?php echo get_category_link(101); ?>">
                <span>View All Politics</span>
                <i class="fa fa-arrow-circle-o-right"></i>
            </a>

        </li>
        <li>
            <a href="<?php echo get_category_link(7); ?>" class="topic_header music">Music</a>
            <?php
                $music_args = array('category_name' => 'music', 'posts_per_page' => 1);
                $music_loop = new WP_Query($music_args);
                if ( $music_loop->have_posts() ) : while ( $music_loop->have_posts() ) : $music_loop->the_post();
            ?>
                <a class="recent_post" href="<?php the_permalink(); ?>">
                    <div class="image_container">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <p><?php the_title(); ?></p>
                </a>
            <?php endwhile; wp_reset_postdata(); endif; ?>

            <a class="read_more" href="<?php echo get_category_link(7); ?>">
                <span>View All Music</span>
                <i class="fa fa-arrow-circle-o-right"></i>
            </a>
        </li>
        <li>
            <a href="<?php echo get_category_link(1891); ?>" class="topic_header health">Health</a>

            <?php
                $health_args = array('category_name' => 'health', 'posts_per_page' => 1);
                $health_loop = new WP_Query($health_args);
                if ( $health_loop->have_posts() ) : while ( $health_loop->have_posts() ) : $health_loop->the_post();
            ?>
                <a class="recent_post" href="<?php the_permalink(); ?>">
                    <div class="image_container">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <p><?php the_title(); ?></p>
                </a>
            <?php endwhile; wp_reset_postdata(); endif; ?>

            <a class="read_more" href="<?php echo get_category_link(1891); ?>">
                <span>View All Health</span>
                <i class="fa fa-arrow-circle-o-right"></i>
            </a>
        </li>
        <li>
            <a href="<?php echo get_category_link(246); ?>" class="topic_header culture">Arts & Culture</a>
            <?php
                $culture_args = array('category_name' => 'culture', 'posts_per_page' => 1);
                $culture_loop = new WP_Query($culture_args);
                if ( $culture_loop->have_posts() ) : while ( $culture_loop->have_posts() ) : $culture_loop->the_post();
            ?>
                <a class="recent_post" href="<?php the_permalink(); ?>">
                    <div class="image_container">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <p><?php the_title(); ?></p>
                </a>
            <?php endwhile; wp_reset_postdata(); endif; ?>

             <a class="read_more" href="<?php echo get_category_link(246); ?>">
                <span>View All Culture</span>
                <i class="fa fa-arrow-circle-o-right"></i>
            </a>
        </li>
        <li>
            <a href="<?php echo get_category_link(1892); ?>" class="topic_header business">Business</a>
            <?php
                $business_args = array('category_name' => 'business', 'posts_per_page' => 1);
                $business_loop = new WP_Query($business_args);
                if ( $business_loop->have_posts() ) : while ( $business_loop->have_posts() ) : $business_loop->the_post();
            ?>
                <a class="recent_post" href="<?php the_permalink(); ?>">
                    <div class="image_container">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <p><?php the_title(); ?></p>
                </a>
            <?php endwhile; wp_reset_postdata(); endif; ?>

            <a class="read_more" href="<?php echo get_category_link(1892); ?>">
                <span>View All Business</span>
                <i class="fa fa-arrow-circle-o-right"></i>
            </a>
        </li>
    </ul>

</div>