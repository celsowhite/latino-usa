<!-- Reporter Information -->

<?php $reporters = get_field('lusa_reporters'); $contributors = get_post_meta(get_the_ID(), '_cmb_contributor_group', true); ?>

<?php if( $reporters ): ?>

    <span class="reporter_info">By

    	<?php $reporter_names = array(); ?>

		<?php foreach( $reporters as $reporter ): ?>

				<?php
					$url = get_the_permalink($reporter->ID);
					$title = get_the_title($reporter->ID); 
					$reporter_names[] = '<a href="' . $url . '">' . $title . '</a>';
				?>

		<?php endforeach; ?>

		<?php

			// Output reporter names with commas and 'and' seperators appropriatley.

			$last  = array_slice($reporter_names, -1);
			$first = join(', ', array_slice($reporter_names, 0, -1));
			$both  = array_filter(array_merge(array($first), $last), 'strlen');
			echo join(' and ', $both);
		?>

	</span>

<?php elseif(empty($reporters) && $contributors): ?>

	<?php get_template_part('template-parts/template', 'contributors'); ?>

<?php else: ?>

	<span class="reporter_info">By <a><?php the_author(); ?></a></span>

<?php endif; ?>