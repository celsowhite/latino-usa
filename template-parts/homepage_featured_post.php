<!-- Featured News Post -->
					
<?php $posts = get_field('homepage_featured_post'); if($posts): ?>

	<?php foreach( $posts as $post ): ?>

		<?php setup_postdata($post); ?>

		<div class="post_container">

			<!-- Title, Reporters & Excerpt -->

			<div class="text">

				<div class="title">

					<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

					<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

					<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

				</div>

				<?php if(get_field('lusa_excerpt')): ?>

					<p><?php the_field('lusa_excerpt'); ?></p>

				<?php endif; ?>

			</div>

			<!-- Featured Image or Video -->

			<?php $post_format = get_post_format(); ?>

			<?php if ($post_format == 'video'): ?>

				<div class="media">
					<?php the_field('lusa_video_embed'); ?>
				</div>

			<?php else: ?>

				<div class="media">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
				</div>

			<?php endif; ?>

		</div>

	<?php endforeach; ?>

	<?php wp_reset_postdata(); ?>

<?php endif; ?>