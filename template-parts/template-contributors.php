<!-- Legacy contributors meta field code -->

<?php $contributors = get_post_meta(get_the_ID(), '_cmb_contributor_group', true); ?>

<?php if (!empty($contributors)): ?>
    <?php
        if (!is_array($contributors)) {
            $contributors = unserialize($contributors);
        }
    ?>

    <?php $contributor_names = array(); ?>

    <span class="reporter_info">By

        <?php foreach($contributors as $contributor): ?>

            <?php 
                $name = $contributor['contributor_name'];
                $lowercasename = strtolower($name);
                $urlname = preg_replace("/[\s]/", "-", $lowercasename);
                $contributor_names[] = '<a href="#">' . $name . '</a>';
            ?>

        <?php endforeach; ?>

        <?php echo implode(' AND ', $contributor_names); ?>

    </span>

<?php endif; ?>