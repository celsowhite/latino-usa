<?php
/**
 * Template part for displaying single posts.
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<!-- Reporter Information -->

		<?php if(get_field('lusa_reporters')): ?>

			<?php $posts = get_field('lusa_reporters'); if($posts): foreach($posts as $post): setup_postdata($post); ?>
				<?php the_title(); ?>
				<?php the_excerpt(); ?>
			<?php endforeach; wp_reset_postdata(); endif; ?>

		<?php endif; ?>


		<div class="entry-meta">
			<?php _s_posted_on(); ?>
		</div>

		<!-- Change the featured media based on the post format -->

		<?php $post_format = get_post_format(); ?>

		<?php if ($post_format == 'audio'): ?>

			<?php the_field('lusa_audio_embed'); ?>

		<?php elseif($post_format == 'video'): ?>

			<?php the_field('lusa_video_embed'); ?>

			<?php the_post_thumbnail(); ?>

		<?php else: ?>

			<?php the_post_thumbnail(); ?>

		<?php endif; ?>

	</header>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', '_s' ),
				'after'  => '</div>',
			) );
		?>
	</div>

	<!-- If apart of an episode then list all segments from that episode -->

	<?php 
	$related_segments_args = array(
		'post_type' => array('lusa_episode'),
		'order' => 'ASC',
		'orderby' => 'menu_order',
		'meta_query' => array(
			array(
				'key' => 'related_segments',
				'value' => '"' . get_the_ID() . '"',
				'compare' => 'LIKE'
			)
		)
	);
	$related_segments_loop = new WP_Query($related_segments_args);
	if ( $related_segments_loop->have_posts() ): while ( $related_segments_loop->have_posts() ): $related_segments_loop -> the_post();
	?>

		<h2>Segments from <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

		<?php $posts = get_field('related_segments'); if($posts): foreach($posts as $post): setup_postdata($post); ?>
			<h2><?php the_title(); ?></h2>
		<?php endforeach; wp_reset_postdata(); endif; ?>

	<?php endwhile; wp_reset_postdata(); endif; ?>

	<footer class="entry-footer">
		<?php _s_entry_footer(); ?>
	</footer>
</article>

