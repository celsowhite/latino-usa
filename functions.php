<?php
/**
 * _s functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package _s
 */

if ( ! function_exists( '_s_setup' ) ) :

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _s_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _s, use a find and replace
	 * to change '_s' to the name of your theme in all the template files
	 */
	load_theme_textdomain( '_s', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/*=== Default image thumbnail size ===*/

	set_post_thumbnail_size(600, 380, true);

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'header' => esc_html__( 'Header', '_s' ),
		'footer' => esc_html__( 'Footer', '_s' ),
		'mobile' => esc_html__( 'Mobile', '_s' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'audio',
		'video',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // _s_setup
add_action( 'after_setup_theme', '_s_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _s_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );


/*==========================================
REGISTER WIDGET AREA
https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
==========================================*/

function _s_widgets_init() {

	/*=== Register Sidebars ===*/

	register_sidebar( array(
		'name'          => esc_html__( 'General Sidebar', '_s' ),
		'id'            => 'lusa_sidebar',
		'description'   => 'General sidebar that is displayed across single posts, topic pages, search and the news page.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Homepage Sidebar', '_s' ),
		'id'            => 'homepage_sidebar',
		'description'   => 'Sidebar that is displayed on the homepage.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'All Episodes Sidebar', '_s' ),
		'id'            => 'all_episodes_sidebar',
		'description'   => 'Sidebar that is displayed on the All Episodes page.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Podcasts Sidebar', '_s' ),
		'id'            => 'podcasts_sidebar',
		'description'   => 'Sidebar that is displayed on the All Podcasts page.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Single Episode Sidebar', '_s' ),
		'id'            => 'single_episode_sidebar',
		'description'   => 'Sidebar that is displayed on single episode page along with the playlist for that episode.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Search Sidebar', '_s' ),
		'id'            => 'search_sidebar',
		'description'   => 'Sidebar that is displayed on the search result page.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	/*=== Register Widgets ===*/

	register_widget( 'lusa_donate_widget' );

	register_widget( 'lusa_social_widget' );

	register_widget( 'lusa_podcast_widget' );

	register_widget( 'lusa_htl_widget' );

	register_widget( 'lusa_latest_news_widget' );
}

add_action( 'widgets_init', '_s_widgets_init' );


/*==========================================
ENQUEUE SCRIPTS AND STYLES
==========================================*/

function _s_scripts() {
	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	/*=== Modernizr ===*/

	wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/modernizr/modernizr_flexbox_detection.min.js','', '', true);

	/*=== Minified Foundation 5 ===*/

	wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation/foundation.min.css' );

	wp_enqueue_style( 'foundation_normalize', get_template_directory_uri() . '/css/foundation/normalize.min.css' );

	/*=== Compiled SCSS File ===*/

	wp_enqueue_style( 'custom_styles', get_template_directory_uri() . '/css/style.css?v=1.0.1' );

	/*=== Flexslider Styles ===*/

	wp_enqueue_style('flexslider', get_template_directory_uri() . '/css/flexslider/flexslider.min.css');

	/*=== Animate CSS ===*/

	wp_enqueue_style('animate_css', get_template_directory_uri() . '/css/animate/animate.min.css');

	/*=== Custom Scripts ===*/

	wp_enqueue_script('_s-scripts', get_template_directory_uri() . '/js/scripts.js', '', '', true);

	/*=== Font Awesome ===*/

	wp_enqueue_script('font-awesome', 'https://use.fontawesome.com/bfa5b116b0.js', '', '', true);

	/*=== FitVids ===*/

	wp_enqueue_script('_s-fitvids', get_template_directory_uri() . '/js/fitvids/fitvids.min.js', '', '', true);

	/*=== Flexslider JS ===*/

	wp_enqueue_script( '_s-flexslider', get_template_directory_uri() . '/js/flexslider/jquery.flexslider-min.js', '','', true);

	/*=== Magnific Popup JS ===*/

	wp_enqueue_script( '_s-magnific', get_template_directory_uri() . '/js/magnific/jquery.magnific-popup.min.js', '','', true);

	/*=== Magnific Popup CSS ===*/

	wp_enqueue_style('magnific', get_template_directory_uri() . '/css/magnific/magnific-popup.min.css');

	/*=== Roboto Google Font ===*/

	wp_enqueue_style('roboto', 'https://fonts.googleapis.com/css?family=Roboto:300,300italic,400,400italic,500,500italic,700,700italic');

	/*=== Roboto Condensed Google Font ===*/

	wp_enqueue_style('roboto_condensed', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300');

	wp_enqueue_script( '_s-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/*=== Wordpress Default Jquery ===*/

	if (!is_admin()) {
		wp_enqueue_script('jquery');
	}

}

add_action( 'wp_enqueue_scripts', '_s_scripts' );

/*==========================================
CUSTOM POST TYPES
==========================================*/

/*=== Episodes ===*/

function custom_post_type_episode() {

	$labels = array(
		'name'                => _x( 'Full Shows', 'Post Type General Name'),
		'singular_name'       => _x( 'Show', 'Post Type Singular Name'),
		'menu_name'           => __( 'Full Shows'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All Shows'),
		'view_item'           => __( 'View Show'),
		'add_new_item'        => __( 'Add New Show'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Show'),
		'update_item'         => __( 'Update Show'),
		'search_items'        => __( 'Search Shows'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);

	$args = array(
		'label'               => __( 'episode'),
		'description'         => __( 'Episodes'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'comments' ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'episode'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-format-audio',
	);

	register_post_type( 'lusa_episode', $args );

}

add_action( 'init', 'custom_post_type_episode', 0 );

/*=== Reporters ===*/

function custom_post_type_reporter() {

	$labels = array(
		'name'                => _x( 'Reporters', 'Post Type General Name'),
		'singular_name'       => _x( 'Reporter', 'Post Type Singular Name'),
		'menu_name'           => __( 'Reporters'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All Reporters'),
		'view_item'           => __( 'View Reporter'),
		'add_new_item'        => __( 'Add New Reporter'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Reporter'),
		'update_item'         => __( 'Update Reporter'),
		'search_items'        => __( 'Search Reporters'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);

	$args = array(
		'label'               => __( 'Reporter'),
		'description'         => __( 'Reporters'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
		'hierarchical'        => false,
		'taxonomies'          => array( '' ),
		'rewrite'             => array('slug' => 'reporter'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-id-alt',
	);

	register_post_type( 'lusa_reporter', $args );

}

add_action( 'init', 'custom_post_type_reporter', 0 );

/*=== Funders ===*/

function custom_post_type_funder() {

	$labels = array(
		'name'                => _x( 'Funders', 'Post Type General Name'),
		'singular_name'       => _x( 'Funder', 'Post Type Singular Name'),
		'menu_name'           => __( 'Funders'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All Funders'),
		'view_item'           => __( 'View Funder'),
		'add_new_item'        => __( 'Add New Funder'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Funder'),
		'update_item'         => __( 'Update Funder'),
		'search_items'        => __( 'Search Funders'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);

	$args = array(
		'label'               => __( 'funder'),
		'description'         => __( 'Funders'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail','revisions', 'excerpt' ),
		'hierarchical'        => false,
		'taxonomies'          => array( '' ),
		'rewrite'             => array('slug' => 'funder'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-admin-users',
	);

	register_post_type( 'lusa_funder', $args );

}

add_action( 'init', 'custom_post_type_funder', 0 );

/*=== Team ===*/

function custom_post_type_team() {

	$labels = array(
		'name'                => _x( 'Team', 'Post Type General Name'),
		'singular_name'       => _x( 'Team Member', 'Post Type Singular Name'),
		'menu_name'           => __( 'Team'),
		'parent_item_colon'   => __( ''),
		'all_items'           => __( 'All Team Members'),
		'view_item'           => __( 'View Team Member'),
		'add_new_item'        => __( 'Add New Team Member'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit Team Member'),
		'update_item'         => __( 'Update Team Member'),
		'search_items'        => __( 'Search Team Members'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash'),
	);

	$args = array(
		'label'               => __( 'Team Member'),
		'description'         => __( 'Latino USA Team Members'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail','revisions' ),
		'hierarchical'        => false,
		'taxonomies'          => array( '' ),
		'rewrite'             => array('slug' => 'team'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-groups',
	);

	register_post_type( 'lusa_team', $args );

}

add_action( 'init', 'custom_post_type_team', 0 );

/*==========================================
CUSTOM TAXONOMY
==========================================*/

/*=== Type Taxonomy for Posts

function add_type_taxonomy() {

  register_taxonomy('lusa_type', 'post', array(

      'hierarchical' => true,
      'labels' => array(
	      'name' => _x( 'Type', 'taxonomy general name' ),
	      'singular_name' => _x( 'Type', 'taxonomy singular name' ),
	      'search_items' =>  __( 'Search Types' ),
	      'all_items' => __( 'All Types' ),
	      'edit_item' => __( 'Edit Type' ),
	      'update_item' => __( 'Update Type' ),
	      'add_new_item' => __( 'Add New Type' ),
	      'new_item_name' => __( 'New Type Name' ),
	      'menu_name' => __( 'Type' ),
    ),

      'rewrite' => array(
      'slug' => 'type',
      'with_front' => true
       ),
  ));
};

add_action( 'init', 'add_type_taxonomy', 0 );

===*/

/*=== Team Category ===*/

function add_team_taxonomy() {

  register_taxonomy('lusa_team_category', 'lusa_team', array(

      'hierarchical' => true,
      'labels' => array(
	      'name' => _x( 'Category', 'taxonomy general name' ),
	      'singular_name' => _x( 'Category', 'taxonomy singular name' ),
	      'search_items' =>  __( 'Search Categories' ),
	      'all_items' => __( 'All Categories' ),
	      'edit_item' => __( 'Edit Category' ),
	      'update_item' => __( 'Update Category' ),
	      'add_new_item' => __( 'Add New Category' ),
	      'new_item_name' => __( 'New Category Name' ),
	      'menu_name' => __( 'Categories' ),
    ),

      'rewrite' => array(
      'slug' => 'type',
      'with_front' => true
       ),
  ));
};

add_action( 'init', 'add_team_taxonomy', 0 );

/*=== Reporter Category ===*/

function add_reporter_taxonomy() {

  register_taxonomy('lusa_reporter_category', 'lusa_reporter', array(

      'hierarchical' => true,
      'labels' => array(
	      'name' => _x( 'Category', 'taxonomy general name' ),
	      'singular_name' => _x( 'Category', 'taxonomy singular name' ),
	      'search_items' =>  __( 'Search Categories' ),
	      'all_items' => __( 'All Categories' ),
	      'edit_item' => __( 'Edit Category' ),
	      'update_item' => __( 'Update Category' ),
	      'add_new_item' => __( 'Add New Category' ),
	      'new_item_name' => __( 'New Category Name' ),
	      'menu_name' => __( 'Categories' ),
    ),

      'rewrite' => array(
      'slug' => 'type',
      'with_front' => true
       ),
  ));
};

add_action( 'init', 'add_reporter_taxonomy', 0 );


/*=== Segment Taxonomy ===*/

function add_segment_taxonomies() {

  register_taxonomy('segment', 'post', array(

      'hierarchical'      => true,
      'labels'            => array(
	      'name'              => _x('Segments', 'taxonomy general name'),
	      'singular_name'     => _x('Segment', 'taxonomy singular name'),
	      'search_items'      => __('Search Segments'),
	      'all_items'         => __('All Segments'),
	      'parent_item'       => __('Parent Segment'),
	      'parent_item_colon' => __('Parent Segment:'),
	      'edit_item'         => __('Edit Segment'),
	      'update_item'       => __('Update Segment'),
	      'add_new_item'      => __('Add New Segment'),
	      'new_item_name'     => __('New Segment Name'),
	      'menu_name'         => __('Segment')
       ),
      'show_ui'           => true,
      'show_admin_column' => true,
      'query_var'         => true,
      'rewrite'           => array('slug' => 'segment'),
  ));

};

add_action( 'init', 'add_segment_taxonomies', 0 );

/*=== Add Categories & Tags to Episode Custom Post Type ===*/

function add_categories_tags() {
    register_taxonomy_for_object_type('category', 'lusa_episode');
    register_taxonomy_for_object_type('post_tag', 'lusa_episode');
}

add_action('init', 'add_categories_tags');

/*================================================================
INCLUDE CUSTOM POST TYPE WITHIN MAIN QUERY FOR CATEGORIES/TAGS
================================================================*/

/* function add_custom_types_to_query( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array('post', 'lusa_episode'));
	return $query;
  }
}
add_filter( 'pre_get_posts', 'add_custom_types_to_query' ); */

/*=============================================
ACF OPTIONS PAGE
=============================================*/

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

/*=============================================
ACF IMPROVED RELATIONSHIP FIELD SEARCH
So searching for posts prioritizes the post title.
=============================================*/

// Add Post Title Like as a WP Query Arg

function title_like_posts_where( $where, &$wp_query ) {
    global $wpdb;
    if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $post_title_like ) ) . '%\'';
    }
    return $where;
}

add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );

// Modify ACF Relationship Search Query to find posts with a title like the search parameter.

function lusa_acf_relationship_query( $args, $field, $post_id ) {
	
	$args['post_title_like'] = $args['s'];
	
    return $args;
    
}

add_filter('acf/fields/relationship/query', 'lusa_acf_relationship_query', 10, 3);

/*=============================================
PAGE EXCERPTS
=============================================*/

function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

add_action( 'init', 'add_excerpts_to_pages' );

/*=============================================
ADD CLASS TO THE_CATEGORY
=============================================*/

function add_class_callback( $result ) {

  $class = strtolower( $result[2] );
  $class = str_replace( ' ', '-', $class );
  // do more sanitazion on the class (slug) like esc_attr() and so on

  $replacement = sprintf( ' class="%s">%s</a>', $class, $result[2] );
  return preg_replace( '#>([^<]+)</a>#Uis', $replacement, $result[0] );

}

function add_category_slug( $html ) {

  $search  = '#<a[^>]+(\>([^<]+)\</a>)#Uuis';
  $html = preg_replace_callback( $search, 'add_class_callback', $html );

  return $html;

}

add_filter( 'the_category', 'add_category_slug', 99, 1 );

/*=============================================
CUSTOM LOGIN SCREEN
=============================================*/

// Change the login logo URL

function my_loginURL() {
    return 'http://latinousa.org';
}

add_filter('login_headerurl', 'my_loginURL');

// Enque the login specific stylesheet for design customizations. CSS file is compiled through compass.

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/css/login.css');
}

add_action('login_enqueue_scripts', 'my_logincustomCSSfile');


/*==========================================
INCLUDES
==========================================*/

/*== Implement the Custom Header feature. ==*/

require get_template_directory() . '/inc/custom-header.php';

/*== Custom template tags for this theme. ==*/

require get_template_directory() . '/inc/template-tags.php';

/*== Custom functions that act independently of the theme templates. ==*/

require get_template_directory() . '/inc/extras.php';

/*== Customizer additions. ==*/

require get_template_directory() . '/inc/customizer.php';

/*== Load Jetpack compatibility file. ==*/

require get_template_directory() . '/inc/jetpack.php';

/*== Old Metabox plugin for listing contributors https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress ==*/

include_once get_template_directory() . '/inc/metabox.php';

/*==========================================
WIDGETS
==========================================*/

/*== Donate Widget ==*/

class lusa_donate_widget extends WP_Widget {

	/*== Register Widget with Wordpress ==*/

	function __construct() {
		parent::__construct(
			'lusa_donate_widget', // Base ID
			__( 'LUSA Donate', 'text_domain' ), // Name
			array( 'description' => __( 'A widget to encourage user donations.', 'text_domain' ), ) // Args
		);
	}

	/*=== Front-end display of widget ===*/

	public function widget( $args, $instance ) {

		$widget_id = $args['widget_id'];
		$title = get_field('donate_widget_title', 'widget_' . $widget_id);
		$text = get_field('donate_widget_text', 'widget_' . $widget_id);
		$image = get_field('donate_widget_image', 'widget_' . $widget_id);

		echo '<div class="widget_area donate_widget">';
		echo "<h3 class='title'>$title</h3>";
		echo "<img src='$image' />";
		echo "<p>$text</p>";
		echo '<a href="' . get_page_link(9883) . '" class="blue_cta">Donate</a>';
		echo '</div>';

		echo $args['before_widget'];
		echo $args['after_widget'];

	}

	/*=== Back-end widget form. ===*/

	public function form( $instance ) {

		/*=== Created through Advanced Custom Fields ===*/

	}

	/*=== Sanitize widget form values as they are saved. ===*/

	public function update( $new_instance, $old_instance ) {

		/*=== Created through Advanced Custom Fields ===*/

	}

};

/*== Social Widget ==*/

class lusa_social_widget extends WP_Widget {

	/*== Register Widget with Wordpress ==*/

	function __construct() {
		parent::__construct(
			'lusa_social_widget', // Base ID
			__( 'LUSA Social', 'text_domain' ), // Name
			array( 'description' => __( 'Tweet @LUSA thoughts.', 'text_domain' ), ) // Args
		);
	}

	/*=== Front-end display of widget ===*/

	public function widget( $args, $instance ) {

		$widget_id = $args['widget_id'];
		$title = get_field('social_widget_title', 'widget_' . $widget_id);
		$text = get_field('social_widget_text', 'widget_' . $widget_id);

		echo '<div class="widget_area social_widget">';
		echo '<h3 class="title">' . $title . '</h3>';
		echo '<p>' . $text . '</p>';
		echo "<a class='twitter_share_nourl blue_cta' text='.@LatinoUSA'>Tweet Us <i class='fa fa-lg fa-twitter'></i></a>";
		echo '</div>';

		echo $args['before_widget'];
		echo $args['after_widget'];

	}

	/*=== Back-end widget form. ===*/

	public function form( $instance ) {

		/*=== Created through Advanced Custom Fields ===*/

	}

	/*=== Sanitize widget form values as they are saved. ===*/

	public function update( $new_instance, $old_instance ) {

		/*=== Created through Advanced Custom Fields ===*/

	}

};

/*== Podcast Widget ==*/

class lusa_podcast_widget extends WP_Widget {

	/*== Register Widget with Wordpress ==*/

	function __construct() {
		parent::__construct(
			'lusa_podcast_widget', // Base ID
			__( 'Marias Podcast Picks', 'text_domain' ), // Name
			array( 'description' => __( 'Top podcast picks from LUSA team', 'text_domain' ), ) // Args
		);
	}

	/*=== Front-end display of widget ===*/

	public function widget( $args, $instance ) {

		$widget_id = $args['widget_id'];
		$image = get_field('podcast_widget_image', 'widget_' . $widget_id);

		$posts = get_field('podcast_widget_picks', 'widget_' . $widget_id);
		if ($posts):
			echo '<div class="widget_area marias_picks">';
				echo '<img src="' . $image . '" class="marias_picks_image"/>';
				echo '<div class="lusa_grid">';
					foreach($posts as $pick):
						$link = get_permalink($pick->ID);
						echo '<div class="half">';
							echo '<a href="' . $link . '">';
								echo '<p class="small black">' . get_the_title($pick->ID) . '</p>';
								echo get_the_post_thumbnail($pick->ID);
							echo '</a>';
						echo '</div>';
					endforeach;
				echo '</div>';
			echo '</div>';
		endif;

		echo $args['before_widget'];
		echo $args['after_widget'];

	}

	/*=== Back-end widget form. ===*/

	public function form( $instance ) {

		/*=== Created through Advanced Custom Fields ===*/

	}

	/*=== Sanitize widget form values as they are saved. ===*/

	public function update( $new_instance, $old_instance ) {

		/*=== Created through Advanced Custom Fields ===*/

	}

};

/*== How To Listen Widget ==*/

class lusa_htl_widget extends WP_Widget {

	/*== Register Widget with Wordpress ==*/

	function __construct() {
		parent::__construct(
			'lusa_html_widget', // Base ID
			__( 'How To Listen', 'text_domain' ), // Name
			array( 'description' => __( 'Module to display how to listen to Latino USA.', 'text_domain' ), ) // Args
		);
	}

	/*=== Front-end display of widget ===*/

	public function widget( $args, $instance ) {

		$widget_id = $args['widget_id'];
		$title = get_field('title', 'widget_' . $widget_id);
		$cta_title = get_field('call_to_action_title', 'widget_' . $widget_id);
		$cta_link = get_field('call_to_action_link', 'widget_' . $widget_id);
						
		echo '<div class="widget_area">';

			echo '<h3 class="title">' . $title . '</h3>';
			
			if(have_rows('links', 'widget_' . $widget_id)):
			
				echo '<ul class="htl_icons">';
					
					while(have_rows('links', 'widget_' . $widget_id)): the_row();
						$link = get_sub_field('link', 'widget_' . $widget_id);
						$logo = get_sub_field('logo', 'widget_' . $widget_id);
						echo '<li><div class="htl_image"><a href="' . $link . '" target="_blank"><img src="' . $logo . '"/></a></div></li>';
					endwhile;

				echo '</ul>';
			
			endif;
			
			if($cta_link):
				echo '<a href="' . $cta_link . '" class="blue_cta">' . $cta_title . '</a>';
			endif;

		echo '</div>';

		echo $args['before_widget'];
		echo $args['after_widget'];

	}

	/*=== Back-end widget form. ===*/

	public function form( $instance ) {
		
		/*=== Created through Advanced Custom Fields ===*/

	}

	/*=== Sanitize widget form values as they are saved. ===*/

	public function update( $new_instance, $old_instance ) {

		/*=== Created through Advanced Custom Fields ===*/

	}

};

/*== Latest News Widget ==*/

class lusa_latest_news_widget extends WP_Widget {

	/*== Register Widget with Wordpress ==*/

	function __construct() {
		parent::__construct(
			'lusa_latest_news_widget', // Base ID
			__( 'LUSA Latest News', 'text_domain' ), // Name
			array( 'description' => __( 'Module to display latest news.', 'text_domain' ), ) // Args
		);
	}

	/*=== Front-end display of widget ===*/

	public function widget( $args, $instance ) {

		echo '<div class="widget_area">';

		echo '<h3 class="title">Latest</h3>';

		$news_args = array('post_type' => 'post', 'posts_per_page' => 3,
				              'tax_query'  => array(
				              	                  array('taxonomy' => 'post_format', 'field' => 'slug', 'terms' => array('post-format-audio'), 'operator' => 'NOT IN'),
				              	              ),
				        );
		$news_loop = new WP_Query($news_args);
		if ( $news_loop->have_posts() ) : while ( $news_loop->have_posts() ) : $news_loop->the_post();

			$title = get_the_title();
			$link = get_the_permalink();
			$image = get_the_post_thumbnail();
			echo '<div class="widget_post">';
				echo '<div class="widget_post_text">';
					$categories = get_the_category();
					if ($categories):
						echo '<ul class="post-categories">';
						    echo '<li>';
						    	echo '<a href="' . esc_url(get_category_link($categories[0]->term_id)) . '"class="' . $categories[0]->slug . '">' . $categories[0]->name . '</a>';
						    echo '</li>';
						echo '</ul>';
					endif;
					echo '<a class="widget_post_title" href="' . $link . '">' . $title . '</a>';
				echo '</div>';
				echo '<a class="widget_post_image" href="' . $link . '">' . $image . '</a>';
			echo '</div>';

			endwhile; wp_reset_postdata(); endif;

		echo '</div>';

		echo $args['before_widget'];
		echo $args['after_widget'];

	}

	/*=== Back-end widget form. ===*/

	public function form( $instance ) {

		echo '<p>Widget custom created. Content will automatically appear when placed in a sidebar.</p>';

	}

	/*=== Sanitize widget form values as they are saved. ===*/

	public function update( $new_instance, $old_instance ) {

		/*=== No fields in this widget ===*/

	}

};

/*==========================================
SHORTCODES
==========================================*/

/*=== Figure Shortcode ===*/
function lusa_figure_shortcode( $atts, $content = null ) {
	require_once('template-parts/simple_html_dom.php');
	$content_html = str_get_html($content);
	$image = null;
	$iframe = null;
	$iframeSrc = null;
	if ($content_html->find('img',0)){
		$image = $content_html->find('img', 0);
		$imageSrc = $image->src;
	}
	if ($content_html->find('iframe',0)){
		$iframe = $content_html->find('iframe',0);
		$iframeSrc = $iframe->src;
	}
	$captionText = trim(strip_tags($content_html));
	$figcaption = !empty($captionText) ? '<figcaption>' . '<p>'. $captionText . '</p>' . '</figcaption>' : null;
	$atts = shortcode_atts(
		array(
			'alt' => 'Image',
			'figcaption' => '',
			'align' => 'center'
		), $atts
	);
	if ($image !== null){
		return '<figure class="align-' . $atts['align'] . '"><img class="img-responsive box-shadow" alt="'. $atts['alt'] .'" src="' . $imageSrc . '">'. $figcaption . '</figure>';
	} else {
		return '<figure class="align-' . $atts['align'] . '"><div class="video_embed"><iframe title="'. $atts['alt'] .'" "class="img-responsive box-shadow" src="' . $iframeSrc . '" frameborder="0" allowfullscreen></iframe></div>'. $figcaption . '</figure>';
	}
}
add_shortcode('lusa_figure', 'lusa_figure_shortcode');


/*=== Header HR Shortcode ===*/
function lusa_section_title_shortcode( $atts, $content = null ) {
	return '<header class="section-title text-align--center text-uppercase"><h3>'. $content .'</h3><hr/></header>';
}
add_shortcode( 'lusa_section_title', 'lusa_section_title_shortcode' );

/*=== Anchor Shortcode ===*/
function lusa_anchor_shortcode( $atts, $content = null ) {
	$contentString = (string) $content;
	if ((strpos($contentString, '#') !== FALSE)) {
		$contentString = str_replace("#", "", $contentString);
	}
	return '<span id="'. $content .'"></span>';
}
add_shortcode( 'lusa_anchor', 'lusa_anchor_shortcode' );

/*=== Blockquote Shortcode ===*/

function lusa_blockquote_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'color' => '',
    ), $atts );

	return '<div class="lusa_blockquote">' . '<div class="bar" style="color:' . $a['color'] . '"></div>' . '<div class="lusa_blockquote_content">' . '<p>'. $content . '</p>' . '</div>' . '</div>';

}

add_shortcode( 'lusaquote', 'lusa_blockquote_shortcode' );

/*=== Twitter Blockquote Shortcode ===*/

function lusa_twitter_blockquote_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'color' => '',
    ), $atts );

	return '<div class="lusa_blockquote twitter_blockquote">' . '<div class="bar" style="color:' . $a['color'] . '"></div>' . '<div class="lusa_blockquote_content">' . '<p>'. $content . '</p>' . '<div class="lusa_blockquote_share">' . '<a href="" via="latinousa" class="twitter_share" text="' . $content . '">Tweet this quote</a>' . '<i class="fa fa-lg fa-twitter"></i>' . '</div>' . '</div>' . '</div>';

}

add_shortcode( 'twitterquote', 'lusa_twitter_blockquote_shortcode' );

/*==========================================
WORDPRESS POPULAR POSTS
Modifications to the HTML markup of WPP
==========================================*/

function my_custom_single_popular_post( $post_html, $p, $instance ){

	$post_cat = get_the_category($p->id);

	if(!empty($post_cat)) {
    	$post_cat = '<ul class="post-categories"><li><a href="' . get_category_link($post_cat[0]->term_id) . '" class="' . strtolower($post_cat[0]->cat_name) . '">' . $post_cat[0]->cat_name . '</a></li></ul>';
    }

    else {
    	$post_cat = "";
    }

    $text = '<div class="widget_post_text">' . $post_cat . '<a class="widget_post_title" href="' . get_the_permalink($p->id) . '">' . $p->title . '</a></div>';

    $image = '<a class="widget_post_image" href="' . get_the_permalink($p->id) . '">' . get_the_post_thumbnail($p->id) . '</a>';

    $output = '<div class="widget_post">' . $text . $image . '</div>';

    return $output;

}

add_filter( 'wpp_post', 'my_custom_single_popular_post', 10, 3 );

/*==========================================
GRAVITY FORMS
==========================================*/

/*=== Enable Credit Card Form Field. Activate when start using Paypal Payments Pro ===

function enable_creditcard( $is_enabled ) {
    return true;
}

add_filter( 'gform_enable_credit_card_field', 'enable_creditcard', 11 );


function change_card_order( $cards ) {
    $cards = array (
	    array (
	        'name' => 'MasterCard',
	        'slug' => 'mastercard',
	        'lengths' => '16',
	        'prefixes' => '51,52,53,54,55',
	        'checksum' => true
	    ),
	    array (
	        'name' => 'Visa',
	        'slug' => 'visa',
	        'lengths' => '13,16',
	        'prefixes' => '4,417500,4917,4913,4508,4844',
	        'checksum' => true
	    ),
	    array (
	        'name' => 'American Express',
	        'slug' => 'amex',
	        'lengths' => '15',
	        'prefixes' => '34,37',
	        'checksum' => true
	    ),
	    array (
	        'name' => 'Discover',
	        'slug' => 'discover',
	        'lengths' => '16',
	        'prefixes' => '6011,622,64,65',
	        'checksum' => true
	    ),
	    array (
	        'name' => 'JCB',
	        'slug' => 'jcb',
	        'lengths' => '16',
	        'prefixes' => '35',
	        'checksum' => true
	    ),
	    array (
	        'name' => 'Maestro',
	        'slug' => 'maestro',
	        'lengths' => '12,13,14,15,16,18,19',
	        'prefixes' => '5018,5020,5038,6304,6759,6761',
	        'checksum' => true
	    )
	);

    return $cards;
}

add_filter( 'gform_creditcard_types', 'change_card_order' );
*/

/*=============================================
RELEVANSSI
Add contributor names to index for each post so we can search by name
and find articles by that contributor.
=============================================*/

function add_custom_fields_relevanssi($content, $post) {

	/*=== If post has reporters. Implode the array of reporters. ===*/

	$reporters = get_field('lusa_reporters');

	$reporter_names = array();

	if (get_field('lusa_reporters')):

		foreach( $reporters as $reporter ):

			$title = get_the_title($reporter->ID);
			$reporter_names[] = $title;

		endforeach;

	endif;

	$reporter_names_list = implode(' ', $reporter_names);

	/*=== If post has funders. Implode the array of funders. ===*/

	$funders = get_field('lusa_funders');

	$funder_names = array();

	if (get_field('lusa_funders')):

		foreach( $funders as $funder ):

			$title = get_the_title($funder->ID);
			$funder_names[] = $title;

		endforeach;

	endif;

	$funder_names_list = implode(' ', $funder_names);

	$content .= " " . $reporter_names_list . $funder_names_list;

	return $content;

}

add_filter('relevanssi_post_content', 'add_custom_fields_relevanssi', 10, 3);

/*==========================================
CUSTOM PAGINATION FOR SINGLE REPORTERS/FUNDERS
==========================================*/

function custom_pagination_link( $label = NULL, $dir = 'next', WP_Query $query = NULL ) {
    if ( is_null( $query ) ) $query = $GLOBALS['wp_query'];
    $max_page = ( int ) $query->max_num_pages;
    // Only one page for the query, do nothing
    if ( $max_page <= 1 ) return;
    $paged = ( int ) $query->get( 'paged' );
    if ( empty( $paged ) ) $paged = 1;
    $target_page = $dir === 'next' ?  $paged + 1 : $paged - 1;
    // If we are in 1st page and required previous or in last page and required next,
    // do nothing
    if ( $target_page < 1 || $target_page > $max_page ) return;
    if ( null === $label )  $label = __( 'Next Page &raquo;' );
    $label = preg_replace( '/&([^#])(?![a-z]{1,8};)/i', '&#038;$1', $label );
    printf( '<a href="%s">%s</a>', get_pagenum_link( $target_page ), $label );
}

/*=== Prevent redirection for reporters single post ===*/

add_action( 'template_redirect', function() {
    if ( is_singular( array('lusa_reporter', 'lusa_funder') ) ) {
        global $wp_query;
        $page = ( int ) $wp_query->get( 'page' );
        if ( $page > 1 ) {
            // convert 'page' to 'paged'
            $query->set( 'page', 1 );
            $query->set( 'paged', $page );
        }
        // prevent redirect
        remove_action( 'template_redirect', 'redirect_canonical' );
    }
}, 0 );

/*==========================================
EXCERPT LENGTH
==========================================*/

function custom_excerpt_length( $length ) {

	return 20;

}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more($more) {

	return '...';

}

add_filter('excerpt_more', 'new_excerpt_more');

/*==========================================
LEGACY AUDIO SHORTCODE CONVERTER
==========================================*/

add_filter('the_content', 'legacy_audio_shortcode_converter', 0);

function legacy_audio_shortcode_converter($content){

	if (is_singular('post') && in_array(get_the_date('Y'), (array('2009','2010','2011','2012','2013')))) {
		return str_replace ( '[audio:', '[audio src=', $content );
	}

	return $content;
}

/*==========================================
LIMIT POST REVISIONS
==========================================*/

function lusa_limit_revisions( $num, $post ) {
    if($post || 'lusa_episode' == $post->post_type) {
    	$num = 3;
    }
    return $num;
}

add_filter( 'wp_revisions_to_keep', 'lusa_limit_revisions', 10, 2 );

/*==========================================
Yoast SEO
==========================================*/

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

