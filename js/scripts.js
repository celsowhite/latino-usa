var $j = jQuery.noConflict();

$j(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

	/*=================================
	MOBILE NAV
	=================================*/
	(function(){
		var
			$win = $j(window),
			nav = { $el: $j('.primary-nav') };
			nav.height = (function() { return nav.$el.outerHeight(true); })();
			nav.activeClass = 'primary-nav--active';
			nav.isOpen = false;
			nav.icon = {
				activeClass: 'fa-times',
				inactiveClass: 'fa-bars'
			}
		if(nav.$el) {
			nav.$el.on('click', function(event){
				var $this = $j(event.target);
				nav.isOpen = !nav.isOpen;
				nav.isOpen ? nav.$el.addClass(nav.activeClass) : nav.$el.removeClass(nav.activeClass);
				nav.$el.find('.fa')
					.toggleClass(nav.icon.inactiveClass)
					.toggleClass(nav.icon.activeClass);
			});
		}
	})();

	/*=================================
	STICKY NAV
	=================================*/
	(function(){
		var
			$win = $j(window),
			nav = { $el: $j('.sticky-nav') };
			nav.height = (function() { return nav.$el.outerHeight(true); })();
			nav.activeClass = 'sticky-nav--active';
		if(nav.$el){
			$win.bind('scroll', function(){
				if ($win.scrollTop() > nav.height) {
					nav.$el.addClass(nav.activeClass);
				} else {
					nav.$el.removeClass(nav.activeClass);
					// $win.trigger('resize');
				}
			}).trigger('scroll');
		}
	})();


	/*=================================
	ANIMATE SCROLLTO
	=================================*/
  $j(function() {
    $j('a[href*="#"]:not([href="#"])').click(function() {
			var nav = { $el: $j('.sticky-nav') };
			nav.height = (function() { return nav.$el.outerHeight(true); })();
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $j(this.hash);
        target = target.length ? target : $j('[id="' + this.hash.slice(1) +'"]');
        if (target.length) {
          $j('html, body').animate({
            scrollTop: target.offset().top - (!!nav ? nav.height : null)
          }, 1000);
          return false;
        }
      }
    });
  });
	/*=================================
	NPR EMBED WRAP
	=================================*/

	$j('iframe[src*="npr"]').wrap("<div class='npr_embed'></div>");

	/*=================================
	SOCIAL SHARE
	This site uses Cresta Social Share Plugin for sharing posts and these custom scripts for the blockquote.
	=================================*/

	/*== Facebook Share ==*/

	$j('a.fb_share').click(function(e) {

		e.preventDefault();

		var loc = $j(this).attr('href');

		window.open('http://www.facebook.com/sharer.php?u=' + loc,'facebookwindow','height=450, width=550, top='+($j(window).height()/2 - 225) +', left='+$j(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

	});

	/*== Twitter Share ==*/

	$j('a.twitter_share').click(function(e){

	    e.preventDefault();

	    var loc = $j(this).attr('href');

	    var text = encodeURIComponent($j(this).attr('text'));

	    window.open('http://twitter.com/share?url=' + loc + '&text=' + text, 'twitterwindow', 'height=450, width=550, top='+($j(window).height()/2 - 225) +', left='+$j(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

	});

	$j('a.twitter_share_nourl').click(function(e){

	    e.preventDefault();

	    var text = encodeURIComponent($j(this).attr('text'));

	    window.open('http://twitter.com/share?url=d&text=' + text, 'twitterwindow', 'height=450, width=550, top='+($j(window).height()/2 - 225) +', left='+$j(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

	});

	/*=== LinkedIn Share ===*/

	$j('a.linkedin_share').click(function(e){

		e.preventDefault();

		var loc = $j(this).attr('href');

		var title = encodeURIComponent($j(this).attr('title'));

		var excerpt  = encodeURIComponent($j(this).attr('excerpt'));

		window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + loc + '&title=' + title + '&summary=' + excerpt);

	});

	/*=================================
	SCROLLING SOCIAL BAR
	=================================*/

	// Define variables

	var single_post = $j('.single_post_main');

	var single_share_icons = $j('.single_post_share_icons');

	// Function to control the scroll position of the social links

	var socialScroll = function() {
		var scroll = $j(window).scrollTop();
    	var endofdiv = single_post.offset().top + single_post.height() - 200;

    	if (scroll < single_post.offset().top) {

    		single_share_icons.addClass('regular');
    		single_share_icons.removeClass('fixed');

    	}

    	else if ((scroll > single_post.offset().top) && (scroll <= endofdiv)) {

    		single_share_icons.removeClass('regular');
    		single_share_icons.removeClass('waiting');
    		single_share_icons.addClass('fixed');

    	}

    	else if (scroll >= endofdiv) {

    		single_share_icons.removeClass('fixed');
    		single_share_icons.addClass('waiting');

    	}
	};

	// Only fire if on the single post page

	if (single_post.length) {

		$j(window).scroll(function() {

			socialScroll();

	    });

	    // Set the initial height of the social bar while waiting for the main content to load

	    $j('.single_post_share').css({

	    	'height': '1000px',

	    });

	    // Set the height of the social bar to the correct height once the main content has loaded (assuming 5s)

	   	var setSocialHeight = function() {

	   		$j('.single_post_share').css({

		    	'height': $j('.single_post_content').height(),

		    });
	   	};

	    setTimeout(setSocialHeight,5000);

	};

	/*=================================
	RESPONSIVE VIDEOS
	=================================*/

	/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

	$j('iframe[src*="youtube.com"], iframe[src*="vimeo.com"], iframe[src*="ustream.tv"], iframe[src*="democracynow.org"], iframe[src*="facebook.com"]').wrap("<div class='video_embed'/>");

	/*=== fitVids.js ===*/

	$j(".video_embed").fitVids({
		customSelector: "iframe[src^='http://www.ustream.tv'], iframe[src*='democracynow.org'], iframe[src*='facebook.com']",
	});

	/*=================================
	MAGNIFIC POPUP
	=================================*/

	$j('.team_popup').magnificPopup({
	  type: 'inline',
      midClick: true
	});

	/*=================================
	TOPICS DRAWER
	=================================*/

	$j('.topics_link').click(function(){
		$j('.search_drawer').removeClass('open');
		$j('.topics_drawer').toggleClass('open');
		$j('.menu-item i.fa-caret-down').toggleClass('rotate');
	});

	/*=================================
	SEARCH DRAWER
	=================================*/

	$j('.search_link').click(function(){
		$j('.topics_drawer').removeClass('open');
		$j('#mobile_hidden_menu').removeClass('open');
		$j('.search_drawer').toggleClass('open');
		$j('.search_drawer input.search-field').focus();
	});

	/*=================================
	MOBILE MENU DRAWER
	=================================*/

	$j('.mobile_menu_link').click(function(e){
		e.preventDefault();
		$j('.search_drawer').removeClass('open');
		$j('#mobile_hidden_menu').toggleClass('open');
	});

	/*=================================
	DONATION FORM FUNCTIONALITY
	=================================*/

	$j('.frequency_selection li.gchoice_3_14_0').addClass('frequency_on');

	$j('.frequency_selection li.gchoice_3_14_0').click(function(){

		$j('.frequency_selection li.gchoice_3_14_0').addClass('frequency_on');
		$j('.frequency_selection li.gchoice_3_14_1').removeClass('frequency_on');

	});

	$j('.frequency_selection li.gchoice_3_14_1').click(function(){

		$j('.frequency_selection li.gchoice_3_14_0').removeClass('frequency_on');
		$j('.frequency_selection li.gchoice_3_14_1').addClass('frequency_on');

	});

	/*=== Disable enter on regular form fields ===*/

	$j(document).on( 'keypress', '.gform_wrapper', function (e) {
	    var code = e.keyCode || e.which;
	    if ( code == 13 && ! $j( e.target ).is( 'textarea,input[type="submit"],input[type="button"]' ) ) {
	        e.preventDefault();
	        return false;
	    }
	});

});

$j(window).load(function() {

	// Call these functions when the complete page (html and images) has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

	// Example Flexslider Setup

    /* $j('.story_slider').flexslider({
    	animation: "fade",
    	controlNav: false
    }); */

});
