<?php
/*
Template Name: News
*/

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="lusa_grid">

				<p class="inpage_header">News</p>

				<div class="main_column_left">

					<!-- News Feed -->

					<?php
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$news_args = array('post_type' => 'post', 'posts_per_page' => 10, 'paged' => $paged, 
									 'tax_query' => array(
													 	array('taxonomy' => 'post_format', 'field' => 'slug', 'terms' => array('post-format-audio'), 'operator' => 'NOT IN')
													),
									);
						$news_loop = new WP_Query($news_args);
						if ($news_loop->have_posts()): $post_count = 0; while ($news_loop->have_posts()): $news_loop->the_post();
					?>

						<?php if($post_count==0): ?>

							<div class="post_container_large">

								<div class="top_title">

									<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

								</div>

								<!-- Featured Image or Video -->

								<div class="media">

									<?php $post_format = get_post_format(); ?>

									<?php if ($post_format == 'video'): ?>

										<?php the_field('lusa_video_embed'); ?>

									<?php else: ?>

										<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail('full'); ?>
										</a>

									<?php endif; ?>

								</div>

								<div class="text">

									<div class="title">

										<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

										<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

									</div>

									<?php if(get_field('lusa_excerpt')): ?>

										<p><?php the_field('lusa_excerpt'); ?></p>

									<?php endif; ?>

									<a class="read_more" href="<?php the_permalink(); ?>">
						            	<span>Read More</span>
						            	<i class="fa fa-arrow-circle-o-right"></i>
						        	</a>

								</div>

							</div>

						<?php else: ?>

							<div class="post_container">

								<!-- Title, Reporters & Excerpt -->

								<div class="text">

									<div class="title">

										<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

										<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

										<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

									</div>

									<?php if(get_field('lusa_excerpt')): ?>

										<p><?php the_field('lusa_excerpt'); ?></p>

									<?php endif; ?>

									<a class="read_more" href="<?php the_permalink(); ?>">
						            	<span>Read More</span>
						            	<i class="fa fa-arrow-circle-o-right"></i>
						        	</a>

								</div>

								<!-- Featured Image or Video -->

								<div class="media">

									<?php $post_format = get_post_format(); ?>

									<?php if ($post_format == 'video'): ?>

										<?php the_field('lusa_video_embed'); ?>

									<?php else: ?>

										<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail(); ?>
										</a>

									<?php endif; ?>

								</div>

							</div>

						<?php endif; ?>

					<?php $post_count++; endwhile; ?> 

					<div class="lusa_pagination">

						<span class="previous_posts">
							<?php echo get_previous_posts_link('<i class="fa fa-arrow-circle-o-left"></i> Newer'); ?>
						</span>

						<span class="next_posts">
							<?php echo get_next_posts_link('Older <i class="fa fa-arrow-circle-o-right"></i>', $news_loop->max_num_pages); ?>
						</span>

					</div>
					
					<?php wp_reset_postdata(); endif; ?>

				</div>

				<div class="sidebar_right">

					<?php dynamic_sidebar('lusa_sidebar'); ?>

				</div>

			</div>
		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
