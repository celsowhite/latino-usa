<?php
/*
Template Name: Episodes
*/

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="lusa_grid">

				<p class="inpage_header">Listen to our full shows:</p>

				<div class="main_column_left">
					
					<?php
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$episode_args = array('post_type' => 'lusa_episode', 'posts_per_page' => 5, 'paged' => $paged, 'order' => 'DSC', 'order_by' => 'date');
						$episode_loop = new WP_Query($episode_args);
						if ( $episode_loop->have_posts() ) : $post_count = 0; while ( $episode_loop->have_posts() ) : $episode_loop->the_post();
					?>
						<?php if($post_count==0): ?>

							<div class="post_container_large">

								<div class="top_title">

									<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

								</div>

								<!-- Featured Image -->

								<div class="media">

									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('full'); ?>
									</a>

								</div>

								<div class="text">

									<div class="title">

										<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

										<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

									</div>

									<?php if(get_field('lusa_excerpt')): ?>

										<p><?php the_field('lusa_excerpt'); ?></p>

									<?php endif; ?>

									<a class="read_more" href="<?php the_permalink(); ?>">
						            	<span>Read More</span>
						            	<i class="fa fa-arrow-circle-o-right"></i>
						        	</a>

								</div>

								<!-- Audio Embed -->

								<?php the_field('lusa_audio_embed'); ?>

							</div>

						<?php else: ?>
					
							<div class="post_container">
							
								<div class="text">

									<div class="title">

										<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

										<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

										<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

									</div>

									<?php if(get_field('lusa_excerpt')): ?>

										<p><?php the_field('lusa_excerpt'); ?></p>

									<?php endif; ?>

									<a class="read_more" href="<?php the_permalink(); ?>">
						            	<span>Read More</span>
						            	<i class="fa fa-arrow-circle-o-right"></i>
						        	</a>

								</div>

								<!-- Featured Image -->
								
								<div class="media">

									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>

								</div>

								<!-- Audio Embed -->

								<?php the_field('lusa_audio_embed'); ?>

							</div>

						<?php endif; ?>

					<?php $post_count++; endwhile; ?>

					<div class="lusa_pagination">

						<span class="previous_posts">
							<?php echo get_previous_posts_link('<i class="fa fa-arrow-circle-o-left"></i> Newer'); ?>
						</span>

						<span class="next_posts">
							<?php echo get_next_posts_link('Older <i class="fa fa-arrow-circle-o-right"></i>', $episode_loop->max_num_pages); ?>
						</span>

					</div>

					<?php wp_reset_postdata(); endif; ?>

				</div>

				<div class="sidebar_right">
					
					<?php dynamic_sidebar('all_episodes_sidebar'); ?>

				</div>

			</div>

		<?php endwhile;?>

	</main>

<?php get_footer(); ?>
