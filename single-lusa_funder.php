<?php
/**
 * The template for displaying all single reporter pages with posts from that reporter.
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Reporter Information -->

			<div class="lusa_grid">

				<div class="image_column">

					<a class="team_member_image" href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('thumbnail'); ?>
					</a>

				</div>

				<div class="details_column">

					<div class="reporter_information">

						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
						<?php if(get_field('funder_website')): ?>
							<a href="<?php the_field('funder_website');?>">View Website</a>
						<?php endif; ?>

					</div>

				</div>

			</div>

			<div class="lusa_grid">

				<p class="inpage_header">Stories produced thanks to generous support from <?php the_title(); ?></p>

				<div class="main_column_left">

					<!-- Funder Episodes/Posts -->

					<?php if(get_field('lusa_funder_stories')): ?>

						<?php 

							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

							$ids = get_field('lusa_funder_stories', false, false);

							$funder_posts_args = array(
								'posts_per_page' => 20,
								'post__in'		 => $ids,
								'order'          => 'DSC',
								'orderby'        => 'date',
								'paged'          => $paged,
								'post_type'      => array('post', 'lusa_episode', 'page')
							);

							$funder_posts_loop = new WP_Query($funder_posts_args);

							if ( $funder_posts_loop->have_posts() ): while ( $funder_posts_loop->have_posts() ): $funder_posts_loop -> the_post();
							
						?>

							<div class="post_container">

								<div class="top_content">

									<!-- Title, Reporters & Excerpt -->

									<div class="text">

										<div class="title">

											<h2 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

											<?php get_template_part( 'template-parts/reporter', 'loop' ); ?>

											<span class="post_date"><?php echo get_the_date('M j, Y'); ?></span>

										</div>

										<?php if(get_field('lusa_excerpt')): ?>

											<p><?php the_field('lusa_excerpt'); ?></p>

										<?php endif; ?>

									</div>

									<!-- Featured Image or Video -->

									<?php $post_format = get_post_format(); ?>

									<?php if ($post_format == 'video'): ?>

										<div class="media">
											<?php the_field('lusa_video_embed'); ?>
										</div>

									<?php else: ?>

										<div class="media">
											<?php the_post_thumbnail(); ?>
										</div>

									<?php endif; ?>

								</div>

								<!-- Audio Embed -->

								<?php if(get_field('lusa_audio_embed')): ?>

									<?php the_field('lusa_audio_embed'); ?>

								<?php endif; ?>

							</div>

						<?php endwhile; ?>

						<div class="lusa_pagination">

							<span class="previous_posts">
								<?php echo custom_pagination_link( '<i class="fa fa-arrow-circle-o-left"></i> Newer', 'prev', $funder_posts_loop ); ?>
							</span>

							<span class="next_posts">
								<?php echo custom_pagination_link( 'Older <i class="fa fa-arrow-circle-o-right"></i>', 'next', $funder_posts_loop ); ?>
							</span>

						</div>

						<?php wp_reset_postdata(); endif;?>

					<?php else: ?>

					<p></p>

					<?php endif; ?>

				</div>

				<div class="sidebar_right">

					<?php dynamic_sidebar('lusa_sidebar'); ?>

				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>