<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package _s
 */

get_header(); ?>

	<main id="main" class="main_wrapper" role="main">

		<?php if ( have_posts() ) : ?>

			<p class="inpage_header"><?php printf( esc_html__( 'Search Results for: %s', '_s' ), '<span>' . get_search_query() . '</span>' ); ?></p>

			<div class="lusa_grid">

				<div class="main_column_left">

					<?php while ( have_posts() ) : the_post(); ?>

						<div class="post_container">

							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

							<?php the_excerpt(); ?>

						</div>

					<?php endwhile; ?>

					<div class="lusa_pagination">

						<span class="previous_posts">
							<?php echo get_previous_posts_link('<i class="fa fa-arrow-circle-o-left"></i> Previous'); ?>
						</span>

						<span class="next_posts">
							<?php echo get_next_posts_link('More <i class="fa fa-arrow-circle-o-right"></i>', 0); ?>
						</span>

					</div>

				</div>

				<div class="sidebar_right">

					<div class="widget_area widget">

						<h3 class="title">Search</h3>

						<?php get_search_form(); ?>

					</div>

					<?php dynamic_sidebar('search_sidebar'); ?>

				</div>	

			</div>

		<?php else : ?>

			<p class="inpage_header">No results found</p>

			<div class="lusa_grid">

				<div class="main_column_left">

					<div class="post_container">

						<p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>

					</div>

				</div>

				<div class="sidebar_right">

					<div class="widget_area widget">

						<h3 class="title">Search</h3>

						<?php get_search_form(); ?>

					</div>

					<?php dynamic_sidebar('search_sidebar'); ?>

				</div>	

			</div>

		<?php endif; ?>

	</main>

<?php get_footer(); ?>
