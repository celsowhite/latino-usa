http_path = "/"
css_dir = "css"
sass_dir = "scss"
images_dir = "images"
javascripts_dir = "js"
fonts_dir = "fonts"
line_comments = false
relative_assets = true

output_style = :compressed
environment = :development
